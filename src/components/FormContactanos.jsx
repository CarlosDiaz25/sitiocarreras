import React, { useState } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Typography,
  Input,
  Button,
} from "@material-tailwind/react";
import { Formik } from "formik";
import * as Yup from "yup";
import { TextValidation } from "./shared/TextValidation";
import { EnvelopeIcon, PhoneArrowDownLeftIcon, PhoneArrowUpRightIcon, PhoneIcon, UserCircleIcon } from "@heroicons/react/24/solid";
import { seedMessage } from "@/services/solicitudContacto";
import { NotificationCustom } from "./ui/NotificationCustom";

const ContactoSchema = Yup.object().shape({
  nombre: Yup.string().required("Por favor ingrese su nombre"),
  telefono: Yup.string().required("Por favor ingrese su telefono"),
  email: Yup.string()
    .email("correo electrónico invalido")
    .required("Por favor ingrese su correo electrónico"),
  mensaje: Yup.string().required("Por favor ingrese un mensaje"),
});

export const FormContactanos = () => {
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (data, resetForm) => {
    try {
      setLoading(true);

      const response = await seedMessage(data);

      if (response.resultado) {
        resetForm();
        NotificationCustom({
          type: "success",
          message: "Nos pondremos en contacto a la brevedad...",
        });
      } else {
        NotificationCustom({
          type: "error",
          message:
            "Ocurrio un error al intentar contactar favor de intentarlo mas tarde",
        });
      }
    } catch (error) {
      console.log(error);
      NotificationCustom({
        type: "error",
        message: error,
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <Formik
      initialValues={{
        nombre: "",
        telefono: "",
        email: "",
        mensaje: "",
      }}
      validationSchema={ContactoSchema}
      onSubmit={(values, { resetForm }) => {
        handleSubmit(values, resetForm);
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        setFieldTouched,
        isValid,
        handleSubmit,
      }) => (
        <>
          <Card className=" lg:hidden md:hidden sm:hidden   w-96">
            <CardBody>
              <div className="flex justify-center mb-2">
                <img
                  src="./images/logoOficial.png"
                  alt="logo"
                  className="w-32"
                />
              </div>

              <Typography variant="h5" color="blue-gray" className="mb-2">
                ¡Anuncia tu Evento de Carreras aqui!
              </Typography>
              <Typography variant="small">
                ¿Estás organizando un emocionante evento de carreras y buscas
                una plataforma para promocionarlo? ¡Entonces SporPalace carreras
                es el lugar perfecto para ti! En SporPalace carreras, nos
                apasiona proporcionar una plataforma para que los organizadores
                de eventos de carreras promocionen sus competiciones y lleguen a
                una audiencia más amplia de entusiastas corredores.
              </Typography>
            </CardBody>
          </Card>
          <div className="flex justify-center mt-20">
            <Card className="w-96">
              <CardHeader
                variant="gradient"
                color="red"
                className="mb-4 grid h-28 place-items-center "
              >
                <Typography variant="h3" color="white">
                  Contactanos
                </Typography>
              </CardHeader>
              <CardBody className="flex flex-col gap-4">
                <div>
                  <Input
                    size="lg"
                    label="Nombre"
                    name="nombre"
                    value={values.nombre}
                    onChange={handleChange("nombre")}
                    onBlur={() => setFieldTouched("nombre")}
                    icon={<UserCircleIcon className="w-5 h-5" />}
                  />
                  {touched.nombre && errors.nombre && (
                    <TextValidation message={errors.nombre} />
                  )}
                </div>
                <div>
                  <Input
                    size="lg"
                    label="Email"
                    type="email"
                    name="email"
                    value={values.email}
                    onChange={handleChange("email")}
                    onBlur={() => setFieldTouched("email")}
                    icon={<EnvelopeIcon className="w-5 h-5" />}
                  />
                  {touched.email && errors.email && (
                    <TextValidation message={errors.email} />
                  )}
                </div>
                <div>
                  <Input
                    size="lg"
                    label="Telefono"
                    name="telefono"
                    value={values.telefono}
                    onChange={handleChange("telefono")}
                    onBlur={() => setFieldTouched("telefono")}
                    icon={<PhoneIcon className="w-5 h-5" />}
                  />
                  {touched.telefono && errors.telefono && (
                    <TextValidation message={errors.telefono} />
                  )}
                </div>
                <div>
                  <Input
                    size="lg"
                    label="Mensaje"
                    name="mensaje"
                    value={values.mensaje}
                    onChange={handleChange("mensaje")}
                    onBlur={() => setFieldTouched("mensaje")}
                    icon={<EnvelopeIcon className="w-5 h-5" />}
                  />
                  {touched.mensaje && errors.mensaje && (
                    <TextValidation message={errors.mensaje} />
                  )}
                </div>
              </CardBody>
              <CardFooter className="pt-0">
                {!loading ? (
                  <Button
                    className="mt-6 bg-[#0000de]"
                    fullWidth
                    onClick={() => handleSubmit()}
                    disabled={!isValid}
                  >
                    Contactar
                  </Button>
                ) : (
                  <Button disabled={true} fullWidth>
                    Cargando...
                  </Button>
                )}
              </CardFooter>
            </Card>
            <Card className="hidden lg:flex md:flex sm:flex   w-96">
              <CardBody>
                <div className="flex justify-center mb-2">
                  <img
                    src="./images/logoOficial.png"
                    alt="logo"
                    className="w-32"
                  />
                </div>

                <Typography variant="h5" color="blue-gray" className="mb-2">
                  ¡Anuncia tu Evento de Carreras aqui!
                </Typography>
                <Typography variant="small">
                  ¿Estás organizando un emocionante evento de carreras y buscas
                  una plataforma para promocionarlo? ¡Entonces SporPalace
                  carreras es el lugar perfecto para ti! En SporPalace carreras,
                  nos apasiona proporcionar una plataforma para que los
                  organizadores de eventos de carreras promocionen sus
                  competiciones y lleguen a una audiencia más amplia de
                  entusiastas corredores.
                </Typography>
              </CardBody>
            </Card>
          </div>
        </>
      )}
    </Formik>
  );
};
