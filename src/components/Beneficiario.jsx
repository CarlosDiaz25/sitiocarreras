"use client";

import {
  ClipboardDocumentIcon,
  ExclamationCircleIcon,
  HeartIcon,
  NoSymbolIcon,
} from "@heroicons/react/24/solid";
import { Card, CardBody, Typography } from "@material-tailwind/react";

const Beneficiario = () => {
  return (
    <section className="text-gray-600 body-font">
      <div className="container px-5 py-24 mx-auto flex flex-col items-center justify-center">
        <div className="flex flex-wrap w-full  flex-col items-center  text-center">
          <Typography
            variant="h1"
            color="black"
            className="mb-4 text-3xl md:text-4xl lg:text-5xl"
          >
            Beneficiarios
          </Typography>
        </div>
        <Card className="w-full item max-w-[48rem] ">
          <CardBody>
            <div className="flex">
              <div className="flex flex-col justify-center items-center">
                <img
                  src={"./images/Beneficiarios.jpg"}
                  alt="card-image"
                  className="rounded-lg"
                />
              </div>
            </div>
          </CardBody>
        </Card>
      </div>
    </section>
  );
};

export default Beneficiario;
