"use client";

import { getPaquete } from "@/services/paquete";
import {
  Card,
  CardHeader,
  CardBody,
  Typography,
} from "@material-tailwind/react";
import { NotificationCustom } from "./ui/NotificationCustom";
import { useEffect, useState } from "react";

const InformacionPaquete = ({ idcarrera }) => {
  const [paquete, setPaquetes] = useState({ lugaresEntrega: [] });

  useEffect(() => {
    obtenerPaquete();
  }, []);

  const obtenerPaquete = async () => {
    try {
      const response = await getPaquete(idcarrera);

      if (response.resultado) setPaquetes(response.data);
    } catch (error) {
      console.log(error);
      NotificationCustom({
        type: "error",
        message: "Ocurrió un error al obtener el paquete",
      });
    }
  };

  return (
    <section className="text-gray-600 body-font">
      <div className="container  py-24 mx-auto flex flex-col items-center justify-center">
        <div className="flex flex-wrap w-full  flex-col items-center  text-center">
          <Typography variant="h3" color="black">
            Información de paquete
          </Typography>
        </div>
        <img
          src={paquete.rutaImagen}
          className="lg:hidden md:hidden sm:hidden w-44 rounded-lg"
          alt="paquete"
        />
        <Card className="w-full item max-w-[48rem] flex-row">
          <CardHeader
            shadow={false}
            floated={false}
            className="hidden lg:flex md:flex sm:flex flex-wrap w-2/5 shrink-0 rounded-r-none mb-4 justify-center items-center"
          >
            <img
              src={paquete.rutaImagen}
              className="w-44 rounded-lg mb-5"
              alt="lugarEntrega"
            />
            <img
              src={paquete.rutaImagenLugar}
              className="w-60 rounded-lg"
              alt="sportpaalce"
            />
          </CardHeader>
          <CardBody>
            <Typography variant="h5" color="black" className="mb-4 uppercase">
              Paquete de corredor
            </Typography>
            <p>{paquete.descripcion}</p>
            <Typography
              variant="h5"
              color="blue-gray"
              className="mt-5 uppercase"
            >
              Lugar de entrega
            </Typography>

            {paquete.lugaresEntrega.map(
              ({ direccion, fechasEntregas }, index) => (
                <div key={index}>
                  <Typography variant="h6" color="blue-gray">
                    * {direccion}
                  </Typography>
                  {fechasEntregas.map((a,index) => (
                    <Typography key={index} color="gray" className="font-normal">
                      {a}
                    </Typography>
                  ))}
                </div>
              )
            )}

            <Typography variant="h6" color="blue-gray" className="mt-5">
              Fecha limite para apartar la talla de la playera{" "}
              {paquete.fechaLimite}
            </Typography>
            <h6 className="block antialiased tracking-normal font-sans text-base font-semibold leading-relaxed text-red-500 mt-5">
              {paquete.restriccion}
            </h6>
          </CardBody>
        </Card>
      </div>
    </section>
  );
};

export default InformacionPaquete;
