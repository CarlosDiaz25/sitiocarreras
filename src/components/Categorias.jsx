import { getTypeCareer } from "@/services/tipoCarreras";
import React, { useEffect, useState } from "react";
import { NotificationCustom } from "./ui/NotificationCustom";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Collapse,
  Typography,
} from "@material-tailwind/react";
import {
  CheckBadgeIcon,
  CurrencyDollarIcon,
  UserGroupIcon,
} from "@heroicons/react/24/solid";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/free-mode";

import { FreeMode, Pagination } from "swiper/modules";
import Link from "next/link";
import { SubCategorias } from "./SubCategorias";
import { getReward } from "@/services/premiacion";
import { FooterCategoria } from "./FooterCategoria";

export const Categorias = ({ idcarrera }) => {
  const [types, setTypes] = useState([]);

  const [numberCard, setNumberCard] = useState(0);

  const toggleOpen = (id) => {
    setOpen((cur) => !cur);
    setNumberCard(id);
  };

  useEffect(() => {
    obtenerTypes();
  }, []);

  const obtenerTypes = async () => {
    try {
      const response = await getTypeCareer(idcarrera);

      if (response.resultado) setTypes(response.data);
    } catch (error) {
      console.log(error);
      NotificationCustom({
        type: "error",
        message: "Ocurrió un error al obtener las categorias",
      });
    }
  };

  return (
    <>
      <div className="text-center mt-10">
        <Typography variant="h3" color="black">
          Categorías
        </Typography>
      </div>
      <Swiper
        breakpoints={{
          340: {
            slidesPerView: 1,
            spaceBetween: 8,
          },
          700: {
            slidesPerView: 3,
            spaceBetween: 15,
          },
        }}
        freeMode={true}
        pagination={{
          clickable: true,
        }}
        modules={[FreeMode, Pagination]}
        className="mt-10 max-w-[90%] lg:max-w-[80%]"
      >
        {types.map(
          (
            {
              id,
              ruta,
              nombre,
              descripcion,
              precio,
              premiacion,
              premiacionEspecial,
            },
            index
          ) => (
            <SwiperSlide key={index}>
              <Card className="mt-6">
                <CardHeader color="blue-gray" className="relative h-50">
                  <img src={ruta} alt="card-image" />
                </CardHeader>
                <CardBody>
                  <Typography variant="h5" color="blue-gray" className="mb-2">
                    {nombre}
                  </Typography>
                  <Typography variant="h6" color="blue" className="mb-2">
                    Costo ${precio} MXN
                  </Typography>
                  {premiacion === true ? (
                    <Typography
                      variant="h6"
                      color="red"
                      className="flex mb-2 gap-2 items-center"
                    >
                      <CurrencyDollarIcon className="w-6 h-6" />
                      Carrera con premiación
                    </Typography>
                  ) : (
                    <></>
                  )}

                  <Typography>
                    {descripcion.substring(0, 200) + "..."}
                  </Typography>
                </CardBody>
                <CardFooter className="pt-0">
                  <FooterCategoria
                    id={id}
                    premiacion={premiacion}
                    premiacionEspecial={premiacionEspecial}
                  />
                </CardFooter>
              </Card>
            </SwiperSlide>
          )
        )}
      </Swiper>
    </>
  );
};
