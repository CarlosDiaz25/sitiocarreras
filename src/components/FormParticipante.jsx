import { Button, Input, Option, Select } from "@material-tailwind/react";
import { useState } from "react";
import { useSession } from "next-auth/react";
import { saveParticipante } from "@/services/participante";
import { FullScreenLoading } from "./ui/FullScreenLoading";
import * as Yup from "yup";
import { Formik } from "formik";
import { UserCircleIcon } from "@heroicons/react/24/solid";
import { TextValidation } from "./shared/TextValidation";
import { NotificationCustom } from "./ui/NotificationCustom";

const ParticipanteSchema = Yup.object().shape({
  nombre: Yup.string().required("Favor de ingresar un nombre"),
  apellidoPaterno: Yup.string().required(
    "Favor de ingresar un apellido paterno"
  ),
  fechaNacimiento: Yup.string().required(
    "Favor de ingresar una fecha de nacimiento"
  ),
  genero: Yup.string().required("Favor de seleccionar un genero"),
});

const FormParticipante = ({ onRegister = () => {} }) => {
  const { data: session } = useSession();
  const [loading, setLoading] = useState(false);

  const optionsGenero = [
    { value: "MASCULINO", text: "MASCULINO" },
    { value: "FEMENINO", text: "FEMENINO" },
    { value: "OTRO", text: "OTRO" },
  ];

  const handleSubmit = async (data,resetForm) => {
    try {
      setLoading(true);

      const participanteRegistrado = await saveParticipante({
        ...data,
        usuarioid: session.user.id,
      });

      if (participanteRegistrado.resultado) {
        resetForm();
        NotificationCustom({
          type: "success",
          message: "El participante se registró con éxito",
        });

        onRegister(participanteRegistrado.data);
      } else {
        NotificationCustom({
          type: "error",
          message: participanteRegistrado.data,
        });
      }
    } catch (error) {
      console.log(error);
      NotificationCustom({
        type: "error",
        message: error,
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <Formik
      initialValues={{
        usuarioid: "",
        nombre: "",
        apellidoPaterno: "",
        apellidoMaterno: "",
        fechaNacimiento: "",
        genero: "",
      }}
      validationSchema={ParticipanteSchema}
      onSubmit={(values, { resetForm }) =>{ handleSubmit(values,resetForm); } }
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        setFieldTouched,
        isValid,
        handleSubmit
      }) => (
        <form className="mt-8 mb-2 w-80 max-w-screen-lg sm:w-96">
          <div className="mb-4 flex flex-col gap-6">
            <div>
              <Input
                label="Nombre"
                name="nombre"
                size="lg"
                className="uppercase"
                value={values.nombre}
                onChange={handleChange("nombre")}
                onBlur={() => setFieldTouched("nombre")}
                icon={<UserCircleIcon className="w-5 h-5" />}
              />
              {touched.nombre && errors.nombre && (
                <TextValidation message={errors.nombre} />
              )}
            </div>
            <div>
              <Input
                label="Apellido Paterno"
                name="apellidoPaterno"
                size="lg"
                className="uppercase"
                value={values.apellidoPaterno}
                onChange={handleChange("apellidoPaterno")}
                onBlur={() => setFieldTouched("apellidoPaterno")}
                icon={<UserCircleIcon className="w-5 h-5" />}
              />
              {touched.apellidoPaterno && errors.apellidoPaterno && (
                <TextValidation message={errors.apellidoPaterno} />
              )}
            </div>
            <div>
              <Input
                label="Apellido Materno"
                name="apellidoMaterno"
                size="lg"
                className="uppercase"
                value={values.apellidoMaterno}
                onChange={handleChange("apellidoMaterno")}
                onBlur={() => setFieldTouched("apellidoMaterno")}
                icon={<UserCircleIcon className="w-5 h-5" />}
              />
              {touched.apellidoMaterno && errors.apellidoMaterno && (
                <TextValidation message={errors.apellidoMaterno} />
              )}
            </div>
            <div>
              <Input
                type="date"
                label="Fecha Nacimiento"
                size="lg"
                name="fechaNacimiento"
                value={values.fechaNacimiento}
                onChange={handleChange("fechaNacimiento")}
                onBlur={() => setFieldTouched("fechaNacimiento")}
              />
              {touched.fechaNacimiento && errors.fechaNacimiento && (
                <TextValidation message={errors.fechaNacimiento} />
              )}
            </div>
            <div>
              <Select
                label="Selecciona un genero"
                name="genero"
                onChange={handleChange("genero")}
                value={values.genero}
                onBlur={() => setFieldTouched("genero")}
              >
                {optionsGenero.map((option) => (
                  <Option key={option.value} value={option.value}>
                    {option.text}
                  </Option>
                ))}
              </Select>
              {touched.genero && errors.genero && (
                <TextValidation message={errors.genero} />
              )}
            </div>
            {!loading ? (
              <Button
                className="mt-6 bg-[#0000de]"
                fullWidth
                onClick={() => handleSubmit()}
                disabled={!isValid}
              >
                Registrar
              </Button>
            ) : (
              <Button disabled={true} fullWidth>
                Cargando...
              </Button>
            )}
          </div>
          {loading ? <FullScreenLoading /> : <></>}
        </form>
      )}
    </Formik>
  );
};

export default FormParticipante;
