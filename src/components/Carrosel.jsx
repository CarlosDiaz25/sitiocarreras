"use client";

import React from "react";
import { Carousel, Typography, Button } from "@material-tailwind/react";
import Link from "next/link";

const CarroselSection = ({ carreras, allCarer = true }) => {
  return (
    <div className="flex flex-col items-center">
      <Carousel className="rounded-xl">
        {carreras.map(({ id, idcarrera, ruta, titulo, descripcion }) => (
          <div key={id} className="relative h-full w-full">
            <img
              src={ruta}
              alt="image 1"
              className="h-full w-full object-cover"
            />
            <div className="hidden lg:flex md:flex">
              <div className="absolute inset-0 grid h-full w-full place-items-center bg-black/50">
                <div className="w-3/4 text-center md:w-2/4">
                  <Typography
                    variant="h1"
                    color="white"
                    className="mb-4 text-3xl md:text-4xl lg:text-5xl"
                  >
                    {titulo}
                  </Typography>

                  <Typography variant="h3" color="white" className="mb-12">
                    {descripcion}
                  </Typography>
                  <div className="flex justify-center gap-2">
                    <div className="flex justify-between">
                      <Link href="/boleto" passHref>
                        <Button size="lg" className="bg-[#0000de] mr-5">
                          Inscribirme
                        </Button>
                      </Link>
                      {allCarer === true ? (
                        <Link
                          href={`/${encodeURIComponent(idcarrera)}`}
                          passHref
                        >
                          <Button size="lg" color="red">
                            Saber mas..
                          </Button>
                        </Link>
                      ) : <></>}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </Carousel>
    </div>
  );
};

export default CarroselSection;
