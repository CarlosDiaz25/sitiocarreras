import { Card, Input, Button, Typography } from "@material-tailwind/react";
import { useState } from "react";
import { FullScreenLoading } from "../ui/FullScreenLoading";
import { recuperarContrasenia } from "@/services/usuario";
import { Formik } from "formik";
import { TextValidation } from "../shared/TextValidation";
import * as Yup from "yup";
import { UserCircleIcon } from "@heroicons/react/24/solid";
import { NotificationCustom } from "../ui/NotificationCustom";

const RecoverPasswordSchema = Yup.object().shape({
  correoElectronico: Yup.string()
    .email("Correo electronico invalido")
    .required("Por favor ingrese su correo electronico"),
});

export default function RecoverAccount() {
  const [loading, setLoading] = useState(false);

  const onRecuperarContasenia = async (correoElectronico) => {
    try {
      setLoading(true);

      const response = await recuperarContrasenia(correoElectronico);

      if (!response.resultado)
        NotificationCustom({
          type: "error",
          message: response.data,
        });
      else
        NotificationCustom({
          type: "success",
          message:
            "La nueva contraseña se envió a tu correo electrónico, favor de validar",
        });
    } catch (error) {
      if (error)
        NotificationCustom({
          type: "error",
          message: { error },
        });
    } finally {
      setLoading(false);
    }
  };

  return (
    <Formik
      initialValues={{
        correoElectronico: "",
      }}
      validationSchema={RecoverPasswordSchema}
      onSubmit={(values) => onRecuperarContasenia(values.correoElectronico)}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        setFieldTouched,
        isValid,
        handleSubmit,
      }) => (
        <Card color="transparent" shadow={false}>
          <Typography variant="h4" color="blue-gray">
            ¿Olvidaste tu contraseña?
          </Typography>
          <Typography color="gray" className="mt-1 font-normal">
            Favor de ingresar tu correo electrónico
          </Typography>
          <form className="mt-8 mb-2 w-80 max-w-screen-lg sm:w-96">
            <div className="mb-4 flex flex-col gap-6">
              <Input
                size="lg"
                label="Correo electrónico..."
                name="correoElectronico"
                value={values.correoElectronico}
                onChange={handleChange("correoElectronico")}
                onBlur={() => setFieldTouched("correoElectronico")}
                icon={<UserCircleIcon className="w-5 h-5" />}
              />
              {touched.correoElectronico && errors.correoElectronico && (
                <TextValidation message={errors.correoElectronico} />
              )}
            </div>
            {!loading ? (
              <Button
                className="mt-6 bg-[#0000de]"
                fullWidth
                onClick={() => handleSubmit()}
                disabled={!isValid}
              >
                Recuperar contraseña
              </Button>
            ) : (
              <Button disabled={true} fullWidth>
                Cargando...
              </Button>
            )}
          </form>
          {loading ? <FullScreenLoading /> : <></>}
        </Card>
      )}
    </Formik>
  );
}
