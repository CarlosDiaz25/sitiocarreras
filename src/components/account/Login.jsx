"use client";

import { useRouter } from "next/navigation";
import { useState } from "react";
import { signIn } from "next-auth/react";
import Link from "next/link";
import { Card, Input, Button, Typography } from "@material-tailwind/react";
import { FullScreenLoading } from "../ui/FullScreenLoading";
import { Formik } from "formik";
import { TextValidation } from "../shared/TextValidation";
import * as Yup from "yup";
import {
  EyeIcon,
  EyeSlashIcon,
  UserCircleIcon,
} from "@heroicons/react/24/solid";
import { NotificationCustom } from "../ui/NotificationCustom";

const LoginSchema = Yup.object().shape({
  correoElectronico: Yup.string()
    .email("correo electronico invalido")
    .required("Por favor ingrese su correo electronico"),
  contrasenia: Yup.string().required("Por favor ingrese su contraseña"),
});

const Login = () => {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [secureTextEntry, setSecureTextEntry] = useState(false);

  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const onLoginUser = async (correoElectronico, contrasenia) => {
    setLoading(true);

    const result = await signIn("credentials", {
      correo: correoElectronico,
      contrasenia: contrasenia,
      redirect: false,
    });

    setLoading(false);

    if (result.error) {
      NotificationCustom({
        type: "error",
        message: "Usuario o contraseña inválidos"
      });
      return;
    }

    const url = result.url.split("?");

    if (url.length === 1) router.replace("/");
    else {
      router.replace(url[1].replace("p=", ""));
    }
  };

  return (
    <Formik
      initialValues={{
        correoElectronico: "",
        contrasenia: "",
      }}
      validationSchema={LoginSchema}
      onSubmit={(values) =>
        onLoginUser(values.correoElectronico, values.contrasenia)
      }
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        setFieldTouched,
        isValid,
        handleSubmit,
      }) => (
        <Card color="transparent" shadow={false}>
          <Typography variant="h4" color="blue-gray">
            Bienvenido
          </Typography>
          <Typography color="gray" className="mt-1 font-normal">
            Favor de ingresar sus credenciales
          </Typography>
          <form className="mt-8 mb-2 w-80 max-w-screen-lg sm:w-96">
            <div className="mb-4 flex flex-col gap-6">

              <div>
                <Input
                  size="lg"
                  label="Correo electrónico..."
                  name="correoElectronico"
                  value={values.correoElectronico}
                  onChange={handleChange("correoElectronico")}
                  onBlur={() => setFieldTouched("correoElectronico")}
                  icon={<UserCircleIcon className="w-5 h-5" />}
                />
                {touched.correoElectronico && errors.correoElectronico && (
                  <TextValidation message={errors.correoElectronico} />
                )}
              </div>

              <div>
                <Input
                  type={secureTextEntry ? "text" : "password"}
                  size="lg"
                  label="Contraseña..."
                  name="contrasenia"
                  value={values.contrasenia}
                  onChange={handleChange("contrasenia")}
                  onBlur={() => setFieldTouched("contrasenia")}
                  icon={
                    secureTextEntry ? (
                      <EyeIcon
                        className="w-5 h-5"
                        onClick={toggleSecureEntry}
                      />
                    ) : (
                      <EyeSlashIcon
                        className="w-5 h-5"
                        onClick={toggleSecureEntry}
                      />
                    )
                  }
                />
                {touched.contrasenia && errors.contrasenia && (
                  <TextValidation message={errors.contrasenia} />
                )}
              </div>
            </div>
            {!loading ? (
              <Button
                className="mt-6 bg-[#0000de]"
                fullWidth
                onClick={() => handleSubmit()}
                disabled={!isValid}
              >
                Iniciar sesión
              </Button>
            ) : (
              <Button disabled={true} fullWidth>
                  Cargando...
              </Button>
            )}
            <Typography color="blue" className="mt-4 text-center font-normal">
              ¿No tienes una cuenta?{" "}
              <Link href="/register" className="font-bold text-gray-900">
                Registrarse
              </Link>
            </Typography>
          </form>
          {loading && (<FullScreenLoading />)}
        </Card>
      )}
    </Formik>
  );
};

export default Login;
