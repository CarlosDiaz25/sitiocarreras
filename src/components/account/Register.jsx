import {
  Card,
  Input,
  Button,
  Typography,
  Select,
  Option,
} from "@material-tailwind/react";
import { signIn } from "next-auth/react";
import { useRouter } from "next/navigation";
import { saveUsuario } from "@/services/usuario";
import Link from "next/link";
import { useState } from "react";
import { FullScreenLoading } from "../ui/FullScreenLoading";
import { Formik } from "formik";
import { TextValidation } from "../shared/TextValidation";
import * as Yup from "yup";
import {
  EnvelopeIcon,
  EyeIcon,
  EyeSlashIcon,
  PhoneIcon,
  UserCircleIcon,
} from "@heroicons/react/24/solid";
import { NotificationCustom } from "../ui/NotificationCustom";

const RegisterSchema = Yup.object().shape({
  nombre: Yup.string().required("Por favor ingrese su nombre"),
  apellidoPaterno: Yup.string().required(
    "Por favor ingrese su apellido paterno"
  ),
  telefono: Yup.string().required("Por favor ingrese su telefono"),
  correoElectronico: Yup.string()
    .email("correo electrónico invalido")
    .required("Por favor ingrese su correo electrónico"),
  contrasenia: Yup.string()
    .min(8, "La contraseña debe de tener minimo 8 caracteres")
    .required("Por favor ingrese su contraseña")
    .matches(
      /^(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[#?!@$%&*-]).{8,}$/,
      "La contraseña debe de tener minimo 8 caracteres, una letra mayuscula y un caracter especial"
    ),
  confirmar: Yup.string()
    .min(8, "La contraseña debe de tener minimo 8 caracteres")
    .required("Por favor ingrese su la confirmación de su contraseña")
    .oneOf([Yup.ref("contrasenia")], "Tus contraseñas no coinciden"),
  fechaNacimiento: Yup.date("La fecha es invalida").required(
    "Por favor ingrese su fecha de nacimiento"
  ),
  genero: Yup.string().required("Por favor ingrese su genero"),
});

export default function Register() {
  const router = useRouter();
  const [loading, setLoading] = useState(false);
  const [secureTextEntry, setSecureTextEntry] = useState(false);
  const [secureConfirmationTextEntry, setSecureConfirmationTextEntry] =
    useState(false);

  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const toggleSecureConfirmationEntry = () => {
    setSecureConfirmationTextEntry(!secureConfirmationTextEntry);
  };

  const optionsGenero = [
    { id: "MASCULINO", nombre: "MASCULINO" },
    { id: "FEMENINO", nombre: "FEMENINO" },
    { id: "OTRO", nombre: "OTRO" },
  ];

  const handleSubmit = async (data) => {
    try {
      setLoading(true);
      
      const sigupResponse = await saveUsuario(data);

      if (!sigupResponse.resultado) {
        NotificationCustom({
          type: "error",
          message: sigupResponse.data,
        });
        return;
      }

      await signIn("credentials", {
        correo: sigupResponse.data.correo,
        contrasenia: sigupResponse.data.contrasenia,
        redirect: false,
      });

      router.replace("/");

    } catch (error) {
      NotificationCustom({
        type: "error",
        message: error,
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <Formik
      initialValues={{
        nombre: "",
        apellidoPaterno: "",
        apellidoMaterno: "",
        telefono: "",
        correoElectronico: "",
        contrasenia: "",
        confirmar: "",
        fechaNacimiento: "",
        genero: "",
      }}
      validationSchema={RegisterSchema}
      onSubmit={(values) => handleSubmit(values)}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        setFieldTouched,
        isValid,
        handleSubmit,
      }) => (
        <Card color="transparent" shadow={false}>
          <Typography variant="h4" color="blue-gray">
            Registrarse
          </Typography>
          <Typography color="gray" className="mt-1 font-normal">
            Favor de ingresar todos los datos solicitados
          </Typography>
          <form className="mt-8 mb-2 w-80 max-w-screen-lg sm:w-96">
            <div className="mb-4 flex flex-col gap-6">
              <div>
                <Input
                  className="uppercase"
                  size="lg"
                  label="Nombre"
                  name="nombre"
                  onChange={handleChange("nombre")}
                  onBlur={() => setFieldTouched("nombre")}
                  icon={<UserCircleIcon className="w-5 h-5" />}
                />
                {touched.nombre && errors.nombre && (
                  <TextValidation message={errors.nombre} />
                )}
              </div>
              <div>
                <Input
                  className="uppercase"
                  size="lg"
                  label="Apellido Paterno"
                  name="apellidoPaterno"
                  onChange={handleChange("apellidoPaterno")}
                  onBlur={() => setFieldTouched("apellidoPaterno")}
                  icon={<UserCircleIcon className="w-5 h-5" />}
                />
                {touched.apellidoPaterno && errors.apellidoPaterno && (
                  <TextValidation message={errors.apellidoPaterno} />
                )}
              </div>
              <div>
                <Input
                  className="uppercase"
                  size="lg"
                  label="Apellido Materno"
                  name="apellidoMaterno"
                  onChange={handleChange("apellidoMaterno")}
                  onBlur={() => setFieldTouched("apellidoMaterno")}
                  icon={<UserCircleIcon className="w-5 h-5" />}
                />
              </div>

              <div>
                <Input
                  className="uppercase"
                  size="lg"
                  label="Teléfono"
                  type="tel"
                  name="telefono"
                  onChange={handleChange("telefono")}
                  onBlur={() => setFieldTouched("telefono")}
                  icon={<PhoneIcon className="w-5 h-5" />}
                />
                {touched.telefono && errors.telefono && (
                  <TextValidation message={errors.telefono} />
                )}
              </div>
              <div>
                <Input
                  size="lg"
                  label="Fecha Nacimiento"
                  type="date"
                  name="fechaNacimiento"
                  onChange={handleChange("fechaNacimiento")}
                  onBlur={() => setFieldTouched("fechaNacimiento")}
                />
                {touched.fechaNacimiento && errors.fechaNacimiento && (
                  <TextValidation message={errors.fechaNacimiento} />
                )}
              </div>

              <div>
                <Select
                  label="Selecciona un género"
                  onChange={handleChange("genero")}
                  onBlur={() => setFieldTouched("genero")}
                >
                  {optionsGenero.map((option) => (
                    <Option key={option.id} value={option.id}>
                      {option.nombre}
                    </Option>
                  ))}
                </Select>
                {touched.genero && errors.genero && (
                  <TextValidation message={errors.genero} />
                )}
              </div>

              <div>
                <Input
                  size="lg"
                  label="Correo electrónico"
                  type="email"
                  name="correoElectronico"
                  onChange={handleChange("correoElectronico")}
                  onBlur={() => setFieldTouched("correoElectronico")}
                  icon={<EnvelopeIcon className="w-5 h-5" />}
                />
                {touched.correoElectronico && errors.correoElectronico && (
                  <TextValidation message={errors.correoElectronico} />
                )}
              </div>

              <div>
                <Input
                  type={secureTextEntry ? "text" : "password"}
                  size="lg"
                  label="Contraseña"
                  name="contrasenia"
                  onChange={handleChange("contrasenia")}
                  onBlur={() => setFieldTouched("contrasenia")}
                  icon={
                    secureTextEntry ? (
                      <EyeIcon
                        className="w-5 h-5"
                        onClick={toggleSecureEntry}
                      />
                    ) : (
                      <EyeSlashIcon
                        className="w-5 h-5"
                        onClick={toggleSecureEntry}
                      />
                    )
                  }
                />
                {touched.contrasenia && errors.contrasenia && (
                  <TextValidation message={errors.contrasenia} />
                )}
              </div>

              <div>
                <Input
                  type={secureConfirmationTextEntry ? "text" : "password"}
                  size="lg"
                  label="Confirmar contraseña"
                  name="confirmar"
                  onChange={handleChange("confirmar")}
                  onBlur={() => setFieldTouched("confirmar")}
                  icon={
                    secureConfirmationTextEntry ? (
                      <EyeIcon
                        className="w-5 h-5"
                        onClick={toggleSecureConfirmationEntry}
                      />
                    ) : (
                      <EyeSlashIcon
                        className="w-5 h-5"
                        onClick={toggleSecureConfirmationEntry}
                      />
                    )
                  }
                />
                {touched.confirmar && errors.confirmar && (
                  <TextValidation message={errors.confirmar} />
                )}
              </div>
            </div>
            {!loading ? (
              <Button
                className="mt-6 bg-[#0000de]"
                fullWidth
                onClick={() => handleSubmit()}
                disabled={!isValid}
              >
                Registrarse
              </Button>
            ) : (
              <Button disabled={true} fullWidth>
                Cargando...
              </Button>
            )}
            <Typography color="blue" className="mt-4 text-center font-normal">
              ¿Ya tienes una cuenta?{" "}
              <Link href="/login" className="font-bold text-gray-900">
                Iniciar sesión
              </Link>
            </Typography>
          </form>
          {loading ? <FullScreenLoading /> : <></>}
        </Card>
      )}
    </Formik>
  );
}
