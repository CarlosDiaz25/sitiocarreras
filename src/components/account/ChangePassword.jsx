import { cambiarContrasenia } from "@/services/usuario";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Chip,
  Dialog,
  DialogBody,
  DialogFooter,
  DialogHeader,
  Input,
  Typography,
} from "@material-tailwind/react";
import React, { useState } from "react";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { FullScreenLoading } from "../ui/FullScreenLoading";
import { useSession } from "next-auth/react";

export default function ChangePassword({ show, handleOpen }) {
  const MySwal = withReactContent(Swal);
  const [mostrarChip, setMostrarChip] = useState(false);
  const [mensajeChip, setMensajeChipp] = useState("");
  const [loading, setLoading] = useState(false);
  const { data: session } = useSession();

  const [data, setData] = useState({
    usuarioId: "",
    contraseniaActual: "",
    contraseniaNueva: "",
    confirmacion: "",
  });

  const handleChangeValueForm = (event) => {
    const { name, value } = event.target;
    setData({
      ...data,
      [name]: value,
    });
  };

  const handleCambiarContraseña = async () => {
    try {
      if (data.contraseniaActual === "") {
        setMensajeChipp("Favor de ingresar su contraseña actual");
        setMostrarChip(true);
        return;
      }

      if (data.contraseniaNueva === "") {
        setMensajeChipp("Favor de ingresar la contraseña nueva");
        setMostrarChip(true);
        return;
      }

      if (data.confirmacion === "") {
        setMensajeChipp("Favor de ingresar la confirmación");
        setMostrarChip(true);
        return;
      }

      if (data.contraseniaNueva !== data.confirmacion) {
        setMensajeChipp("Las contraseñas no coinciden");
        setMostrarChip(true);
        return;
      }
      setLoading(true);

      const response = await cambiarContrasenia({
        usuarioId: session.user.id,
        contraseniaActual: data.contraseniaActual,
        contraseniaNueva: data.contraseniaNueva,
        confirmacion: data.confirmacion,
      });

      if (response.resultado) {
        MySwal.fire({
          icon: "success",
          text: "Proceso generado con éxito",
          confirmButtonColor: "black"
        });

        setMostrarChip(false);

        setData({
          ...data,
          contraseniaActual: "",
          contraseniaNueva: "",
          confirmacion: "",
        });

        handleOpen();
      } else {
        setMensajeChipp(response.data);
        setMostrarChip(true);
        return;
      }
    } catch (error) {
      setMensajeChipp("Ocurrió un error al actualizar la contraseña");
      setMostrarChip(true);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Dialog size={"sm"} handler={handleOpen} open={show}>
      <DialogHeader>Cambiar contraseña</DialogHeader>
      <DialogBody divider>
        <Card className="max-h-[calc(100vh-8rem)] w-full">
          <CardBody className="flex flex-col gap-5">
            {mostrarChip ? (
              <Chip
                variant="ghost"
                color="red"
                size="sm"
                value={mensajeChip}
                className="mb-2"
                icon={
                  <span className="mx-auto mt-1 block h-2 w-2 rounded-full bg-red-900 content-['']" />
                }
              />
            ) : (
              <></>
            )}

            <Input
              type="password"
              size="lg"
              label="Contraseña Actual"
              name="contraseniaActual"
              onChange={handleChangeValueForm}
            />
            <Input
              type="password"
              size="lg"
              label="Contraseña Nueva"
              name="contraseniaNueva"
              onChange={handleChangeValueForm}
            />
            <Input
              type="password"
              size="lg"
              label="Confirmar contraseña"
              onChange={handleChangeValueForm}
              name="confirmacion"
            />
          </CardBody>
          {loading ? <FullScreenLoading /> : <></>}
        </Card>
      </DialogBody>
      <DialogFooter>
        <Button className="bg-[#0000de]" onClick={handleCambiarContraseña}>
          Aceptar
        </Button>
      </DialogFooter>
    </Dialog>
  );
}
