"use client";

import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/free-mode";

import { FreeMode, Pagination } from "swiper/modules";
import { Button, Typography } from "@material-tailwind/react";
import Link from "next/link";

export const CardSliderCarreras = ({ carreras }) => {
  return (
    <div className="m-10">
      <div className="flex flex-wrap w-full  flex-col items-center  text-center">
      <Typography
          variant="h2"
          className="mb-4 mt-10 "
        >
           Competencias disponibles
        </Typography>
      </div>
      <Swiper
        breakpoints={{
          450: {
            slidesPerView: 1,
            spaceBetween: 8,
          },
          700: {
            slidesPerView: 3,
            spaceBetween: 15,
          },
        }}
        freeMode={true}
        pagination={{
          clickable: true,
        }}
        modules={[FreeMode, Pagination]}
        className="lg:max-w-[80%]"
      >
        {carreras.map((item) => (
          <SwiperSlide key={item.id}>
            <div className="flex flex-col gap-6 group relative shadow-lg text-white rounded-xl px-6 py-8 h-[300px] overflow-hidden cursor-pointer">
              <div
                className="absolute inset-0 bg-cover bg-center"
                style={{ backgroundImage: `url(${item.ruta})` }}
              />
              <div className="absolute inset-0 bg-black opacity-10 group-hover:opacity-60" />
              <div className="relative flex flex-col gap-3">
                <h1 className="text-xl lg:text-2xl">{item.titulo}</h1>
                <p className="lg:text-[18px]">{item.direccion}</p>
                <p className="lg:text-[18px]">{item.fecha}</p>

              </div>
              <div className="flex justify-between absolute bottom-5">
                <Link href={`/${encodeURIComponent(item.idcarrera)}`} passHref>
                  <Button color="red">Saber mas..</Button>
                </Link>
              </div>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};
