import { CartContext } from "@/context/cart";
import { currency } from "../../utils";
import { Typography } from "@material-tailwind/react";
import { useContext } from "react";

export const OrderSummary = () => {
  const { numberOfItems, subTotal, costoServicio, total, descuentoAplicado } =
    useContext(CartContext);

  return (
    <div className="flex flex-col">
      <div className="flex w-full justify-between">
        <Typography variant="h6">No. fichas:</Typography>
        <Typography variant="h6">
          {numberOfItems} {numberOfItems > 1 ? "fichas" : "ficha"}
        </Typography>
      </div>
      <div className="flex w-full justify-between">
        <Typography variant="h6">Subtotal:</Typography>
        <Typography variant="h6">{currency.format(subTotal)}</Typography>
      </div>
      {descuentoAplicado > 0 ? (
        <>
          <div className="flex w-full justify-between">
            <Typography color="blue" variant="h6">
              Descuento:
            </Typography>
            <Typography color="blue" variant="h6">
              - {currency.format(descuentoAplicado)}
            </Typography>
          </div>
          <div className="flex w-full justify-between">
            <Typography variant="h6">Total:</Typography>
            <Typography variant="h6">
              {currency.format(subTotal - descuentoAplicado)}
            </Typography>
          </div>
        </>
      ) : (
        <></>
      )}

      <div className="flex w-full justify-between mb-5">
        <Typography variant="h6">Cargo por servicio:</Typography>
        <Typography variant="h6">{currency.format(costoServicio)}</Typography>
      </div>
      <hr />
      <div className="flex w-full justify-between mt-10">
        <Typography variant="h5" color="black">
          Total con comisión:
        </Typography>
        <Typography variant="h5" color="black">
          {currency.format(total)}
        </Typography>
      </div>
    </div>
  );
};
