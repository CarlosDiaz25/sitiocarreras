import { CartContext } from "@/context/cart";
import { TrashIcon } from "@heroicons/react/24/solid";
import {
  Card,
  Chip,
  IconButton,
  Tooltip,
  Typography,
} from "@material-tailwind/react";
import React, { useContext } from "react";

export const CartList = () => {
  const { cart, removeCartFicha } = useContext(CartContext);

  return (
    <div className="flex flex-col mr-5">
      {cart.map((ficha) => (
        <Card
          className="flex flex-row mb-3"
          key={ficha.participante.id + ficha.carrera.id}
        >
          <img
            src={`${ficha.categoria.ruta}`}
            alt="categoria"
            className="hidden lg:flex md:flex sm:flex m-5 rounded-lg w-40"
          />
          <div className="flex w-full ml-2 mt-5 mr-2 justify-between">
            <div className="flex flex-col">
            <div className="flex flex-wrap items-center">
                <Typography variant="h6" color="black" className="mr-2">
                  Carrera:
                </Typography>
                <Typography variant="small" color="black" className="uppercase">
                  {ficha.carrera.nombre}
                </Typography>
              </div>
              <div className="flex flex-wrap items-center">
                <Typography variant="h6" color="black" className="mr-2">
                  Participante:
                </Typography>
                <Typography variant="small" color="black" className="uppercase">
                  {ficha.participante.nombre}{" "}
                  {ficha.participante.apellidoPaterno}{" "}
                  {ficha.participante.apellidoMaterno}
                </Typography>
              </div>
              <div className="flex flex-wrap items-center">
                <Typography variant="h6" color="black" className="mr-2">
                  Talla del paquete:
                </Typography>
                <Typography variant="small" color="black" className="uppercase">
                  {ficha.talla.nombre}
                </Typography>
              </div>

              <div className="flex flex-wrap items-center">
                <Chip
                      variant="ghost"
                      color="blue"
                      size="sm"
                      value= {ficha.categoria.nombre}
                      className="mb-2"
                      icon={
                        <span className="mx-auto mt-1 block h-2 w-2 rounded-full bg-blue-900 content-['']" />
                      }
                    />
              </div>
              <div className="flex flex-wrap items-center">
                <Chip
                      variant="ghost"
                      color="red"
                      size="sm"
                      value= {ficha.subcategoria.nombre}
                      className="mb-2"
                      icon={
                        <span className="mx-auto mt-1 block h-2 w-2 rounded-full bg-red-900 content-['']" />
                      }
                    />
              </div>
            </div>

            <div className="flex flex-col justify-center items-center">
              <Typography variant="h6" color="black" className="mr-2">
                {`$${ficha.subcategoria.costo}`}
              </Typography>
              {ficha.carrera.descuentoAplicado > 0 ? (
                <Typography variant="h6" color="blue" className="mr-2">
                  {`$-${ficha.carrera.descuentoAplicado}`}
                </Typography>
              ) : (
                <></>
              )}
              <Tooltip content="Remover ficha">
                <IconButton color="red" onClick={() => removeCartFicha(ficha)}>
                  <TrashIcon className="h-4 w-4 text-white" />
                </IconButton>
              </Tooltip>
            </div>
          </div>
        </Card>
      ))}
    </div>
  );
};
