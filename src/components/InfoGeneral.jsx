"use client";

import { getDetailCareer } from "@/services/carreras";
import {
  ClipboardDocumentIcon,
} from "@heroicons/react/24/solid";
import { Card, CardBody, Typography } from "@material-tailwind/react";
import { useEffect, useState } from "react";
import { NotificationCustom } from "./ui/NotificationCustom";

const InfoGeneral = ({ idcarrera }) => {
  const [details, setDetails] = useState([]);

  useEffect(() => {
    obtenerDetalles();
  }, []);

  const obtenerDetalles = async () => {
    try {
      const response = await getDetailCareer(idcarrera);

      if (response.resultado) setDetails(response.data);
    } catch (error) {
      console.log(error);
      NotificationCustom({
        type: "error",
        message: "Ocurrió un error al obtener el paquete",
      });
    }
  };

  return (
    <section className="text-gray-600 body-font">
      <div className="container  mx-auto flex flex-col items-center justify-center">
        <div className="flex flex-wrap w-full  flex-col items-center  text-center">
        <Typography variant="h3" color="black">
            Información General
          </Typography>
        </div>

        <Card className="w-full item max-w-[72rem] ">
          <CardBody className="flex">
            <div className="flex flex-col">
              {details.map(({ titulo, descripcion },index) => (
                <div key={index}>
                  <Typography
                    variant="h5"
                    color="black"
                    className="flex  gap-2 mb-4 uppercase"
                  >
                    {titulo}
                  </Typography>
                  <Typography
                    variant="h6"
                    color="gray"
                    className="font-normal mb-4"
                  >
                    {descripcion}
                  </Typography>
                </div>
              ))}
            </div>
          </CardBody>
        </Card>
      </div>
    </section>
  );
};

export default InfoGeneral;
