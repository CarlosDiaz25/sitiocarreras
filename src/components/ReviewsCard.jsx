import React, { useEffect, useState } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Typography,
  Avatar,
} from "@material-tailwind/react";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/free-mode";

import { FreeMode, Pagination } from "swiper/modules";
import { getReviews } from "@/services/reviews";
import { NotificationCustom } from "./ui/NotificationCustom";

function StarIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 24 24"
      fill="currentColor"
      className="h-5 w-5 text-yellow-700"
    >
      <path
        fillRule="evenodd"
        d="M10.788 3.21c.448-1.077 1.976-1.077 2.424 0l2.082 5.007 5.404.433c1.164.093 1.636 1.545.749 2.305l-4.117 3.527 1.257 5.273c.271 1.136-.964 2.033-1.96 1.425L12 18.354 7.373 21.18c-.996.608-2.231-.29-1.96-1.425l1.257-5.273-4.117-3.527c-.887-.76-.415-2.212.749-2.305l5.404-.433 2.082-5.006z"
        clipRule="evenodd"
      />
    </svg>
  );
}

export const ReviewsCard = () => {
  const [reviews, setReviews] = useState([]);

  useEffect(() => {
    obtenerReviews();
  }, []);

  const obtenerReviews = async () => {
    try {
      const response = await getReviews();

      if (response.resultado) setReviews(response.data);
    } catch (error) {
      console.log(error);
      NotificationCustom({
        type: "error",
        message: "Ocurrió un error al obtener las reviews",
      });
    }
  };

  return (
    <div className="mt-20 ">
      <div className="flex flex-wrap w-full  flex-col items-center  text-center">
        <Typography variant="h2" className="mb-4 mt-10 ">
          Opiniones del público
        </Typography>
      </div>
      <Swiper
        breakpoints={{
          340: {
            slidesPerView: 1,
            spaceBetween: 8,
          },
          700: {
            slidesPerView: 3,
            spaceBetween: 15,
          },
        }}
        freeMode={true}
        pagination={{
          clickable: true,
        }}
        modules={[FreeMode, Pagination]}
        className="max-w-[90%] lg:max-w-[80%]"
      >
        {reviews.map(
          ({
            rutaImagen,
            nombre,
            correo,
            numeroEstrellas,
            comentario,
            carrera,
            fecha,
          }, index) => (
            <SwiperSlide key={index}>
              <Card
                color="transparent"
                shadow={false}
                className="w-full max-w-[26rem]"
              >
                <CardHeader
                  color="transparent"
                  floated={false}
                  shadow={false}
                  className="mx-0 flex items-center gap-4 pt-0 pb-8"
                >
                  <Avatar
                    size="lg"
                    variant="circular"
                    src={rutaImagen}
                    alt="tania andrew"
                  />

                  <div className="flex w-full flex-col gap-0.5">
                    <div className="flex items-center justify-between">
                      <Typography variant="h5" color="blue-gray">
                        {nombre}
                      </Typography>
                      <div className="5 flex items-center gap-0">
                        {Array.from({ length: numeroEstrellas }, (_, index) => (
                          <StarIcon  key={index}/>
                        ))}
                      </div>
                    </div>
                    <Typography variant="small" color="blue-gray">
                      {correo}
                    </Typography>
                  </div>
                </CardHeader>
                <CardBody className="mb-6 p-0">
                  <Typography variant="h6" color="blue-gray">
                    {carrera}
                  </Typography>
                  <Typography>{fecha}</Typography>
                  <Typography>{comentario.substring(0, 200) + "..."}</Typography>
                </CardBody>
              </Card>
            </SwiperSlide>
          )
        )}
      </Swiper>
    </div>
  );
};
