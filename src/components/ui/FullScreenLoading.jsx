import { Spinner, Typography } from '@material-tailwind/react'
import React from 'react'

export const FullScreenLoading = () => {
  return (
    <div className="flex flex-col w-full h-cal[100vh-200px] justify-center items-center">
        <Spinner color="blue" />
    </div>
  )
}
