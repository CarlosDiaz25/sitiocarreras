import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

export const NotificationCustom = ({ type, message }) => {
  const MySwal = withReactContent(Swal);

  return MySwal.fire({
    icon: type,
    text: message,
    confirmButtonColor: "blue",
  });
};
