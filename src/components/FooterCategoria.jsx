import { CheckBadgeIcon, TrophyIcon } from "@heroicons/react/24/solid";
import { Button, Collapse } from "@material-tailwind/react";
import Link from "next/link";
import React, { useState } from "react";
import { SubCategorias } from "./SubCategorias";
import { PremiacionEspecial } from "./PremiacionEspecial";

export const FooterCategoria = ({ id, premiacion, premiacionEspecial }) => {
  const [open, setOpen] = useState(false);
  const [openPremiacionEspecial, setOpenPremiacionEspecial] = useState(false);
  return (
    <>
      <div className="flex justify-between flex-wrap">
        <Link href="/boleto" passHref>
          <Button size="lg" className="bg-[#0000de] ">
            Inscribirme
          </Button>
        </Link>

        {premiacion === true ? (
          <Button
            onClick={() => {
              setOpen((cur) => !cur);
            }}
            className=" flex gap-2 justify-center items-center"
            color="red"
          >
            <CheckBadgeIcon className="w-6" />
            Premios..
          </Button>
        ) : (
          <></>
        )}
      </div>
      <div className="mt-5 flex justify-center">
        {premiacionEspecial === true ? (
          <Button
            onClick={() => {
              setOpenPremiacionEspecial((cur) => !cur);
            }}
            className="flex gap-2 justify-center items-center"
            color="red"
          >
            <TrophyIcon className="w-6" />
            Premios Especiales..
          </Button>
        ) : (
          <></>
        )}
      </div>
      {premiacion === true ? (
        <Collapse open={open}>
          <SubCategorias idTipoCarrera={id} />
        </Collapse>
      ) : (
        <></>
      )}

      {premiacionEspecial === true ? (
        <Collapse open={openPremiacionEspecial}>
          <PremiacionEspecial idTipoCarrera={id} />
        </Collapse>
      ) : (
        <></>
      )}
    </>
  );
};
