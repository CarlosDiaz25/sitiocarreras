import React, { useContext, useState } from "react";
import {
  Typography,
  Button,
  Menu,
  MenuHandler,
  MenuList,
  MenuItem,
  Avatar,
} from "@material-tailwind/react";
import {
  UserCircleIcon,
  ChevronDownIcon,
  Cog6ToothIcon,
  InboxArrowDownIcon,
  LifebuoyIcon,
  PowerIcon,
} from "@heroicons/react/24/outline";
import Link from "next/link";
import { AuthContext } from "@/context";

export default function ProfileMenu() {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const { isLoggedIn, logout } = useContext(AuthContext);

  const closeMenu = () => setIsMenuOpen(false);

  const onLogout = () => {
    logout();
    closeMenu();
  };

  return (
    <Menu open={isMenuOpen} handler={setIsMenuOpen} placement="bottom-end">
      <MenuHandler>
        <Button
          variant="text"
          color="blue-gray"
          className="flex items-center gap-1 rounded-full py-0.5 pr-2 pl-0.5 lg:ml-auto"
        >
          <Avatar
            variant="circular"
            size="sm"
            alt="tania andrew"
            className=" p-0.5"
            src="./images/avatar.png"
          />
          <ChevronDownIcon
            strokeWidth={2.5}
            className={`h-3 w-3 transition-transform ${
              isMenuOpen ? "rotate-180" : ""
            }`}
          />
        </Button>
      </MenuHandler>
      <MenuList className="p-1">
        {isLoggedIn ? (
          <>
            <Link href="/perfil" passHref>
              <MenuItem
                key={1}
                onClick={closeMenu}
                className={`flex items-center gap-2 rounded`}
              >
                <UserCircleIcon className="h-4 w-4" />
                <Typography
                  as="span"
                  variant="small"
                  className="font-normal"
                  color="inherit"
                >
                  Mi perfil
                </Typography>
              </MenuItem>
            </Link>
            
            <MenuItem
              key={5}
              onClick={onLogout}
              className={`flex items-center gap-2 rounded hover:bg-red-500/10 focus:bg-red-500/10 active:bg-red-500/10`}
            >
              <PowerIcon className="h-4 w-4 text-red-500" />
              <Typography
                as="span"
                variant="small"
                className="font-normal"
                color="red"
              >
                Cerrar sesión
              </Typography>
            </MenuItem>
          </>
        ) : (
          <>
            <Link href="/login" passHref>
              <MenuItem
                key={4}
                onClick={closeMenu}
                className={`flex items-center gap-2 rounded`}
              >
                <PowerIcon className="h-4 w-4" />
                <Typography
                  as="span"
                  variant="small"
                  className="font-normal"
                  color="inherit"
                >
                  Iniciar sesión
                </Typography>
              </MenuItem>
            </Link>
          </>
        )}
      </MenuList>
    </Menu>
  );
}
