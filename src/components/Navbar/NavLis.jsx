import { ShoppingBagIcon, ShoppingCartIcon, UserCircleIcon } from "@heroicons/react/24/solid";
import { Badge, MenuItem, Typography } from "@material-tailwind/react";
import { useContext } from "react";
import { AuthContext, CartContext } from "@/context";
import Link from "next/link";
import { FullScreenLoading } from "../ui/FullScreenLoading";
import { useSession } from "next-auth/react";

export default function NavList() {
  const { numberOfItems } = useContext(CartContext);
  const { status } = useSession();

  return (
    <ul className="mb-4 mt-2 flex flex-col gap-2 lg:mb-0 lg:mt-0 lg:flex-row lg:items-center">
      <Link href="/boleto" variant="small" color="blue-gray" passHref>
        <MenuItem className="flex items-center gap-2 lg:rounded-full  bg-[#0000de] text-cyan-50">
          <UserCircleIcon className="h-[18px] w-[18px]" />
          Inscribirme
        </MenuItem>
      </Link>

      {status === "authenticated" ? (
        <>
          <Link href="/orders" variant="small" color="blue-gray" passHref>
            <MenuItem className="flex items-center gap-2 lg:rounded-full bg-red-500 text-cyan-50">
              <ShoppingBagIcon className="h-[18px] w-[18px]" />
              Mis compras
            </MenuItem>
          </Link>

          <Link className="w-full lg:w-32" href="/cart" variant="small" color="blue-gray" passHref>
            <div className="w-full  relative inline-flex">
              <button
                className="align-middle select-none font-sans font-bold text-center uppercase transition-all disabled:opacity-50
                           disabled:shadow-none disabled:pointer-events-none text-xs py-3 px-6 lg:rounded-full bg-gray-900 text-white shadow-md\
                         shadow-gray-900/10 hover:shadow-lg hover:shadow-gray-900/20 focus:opacity-[0.85] focus:shadow-none 
                           active:opacity-[0.85] active:shadow-none flex gap-2 h-10 w-full sm:rounded-lg"
                type="button"
                style={{position:"relative"}}
              >
                <ShoppingCartIcon className="h-[18px] w-[18px]" />
                Carrito
              </button>
              <span className="absolute rounded-full py-1 px-1 text-xs font-medium content-[''] leading-none grid place-items-center top-[4%] right-[2%] translate-x-2/4 -translate-y-2/4 bg-red-500 text-white min-w-[24px] min-h-[24px]">
              {numberOfItems > 9 ? "+9" : numberOfItems}
              </span>
            </div>
          </Link>
        </>
      ) : status === "loading" ? (
        <>
          <FullScreenLoading />
        </>
      ) : (
        <></>
      )}
    </ul>
  );
}
