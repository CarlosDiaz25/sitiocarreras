import { getReward } from "@/services/premiacion";
import React, { useEffect, useState } from "react";
import { NotificationCustom } from "./ui/NotificationCustom";
import { List, ListItem, Card, Typography } from "@material-tailwind/react";
import { format } from "@/utils/currency";

export const SubCategorias = ({ idTipoCarrera }) => {
  const [reward, setReward] = useState([]);

  useEffect(() => {
    obtenerReward();
  }, []);

  const obtenerReward = async () => {
    try {
      const response = await getReward(idTipoCarrera);

      if (response.resultado) setReward(response.data);
    } catch (error) {
      console.log(error);
      NotificationCustom({
        type: "error",
        message: "Ocurrió un error al obtener las premiaciones",
      });
    }
  };

  return (
    <div>
      <Card>
        <List>
          {reward.map((a) =>
            a.premiacionTipoCategoria.map(
              ({ categoria, edades, genero, premiaciones }, index) => (
                <ListItem key={index}>
                  <div key={index}>
                    <Typography variant="h5" color="red">
                      {categoria}
                    </Typography>
                    <Typography
                      variant="small"
                      color="gray"
                      className="font-normal"
                    >
                      {genero} {edades}
                    </Typography>
                    {premiaciones.map(({ lugar, premio },index) => (
                      <Typography key={index} variant="h4" className="flex">
                        {lugar}. {format(premio)}
                      </Typography>
                    ))}
                  </div>
                </ListItem>
              )
            )
          )}
        </List>
      </Card>
    </div>
  );
};
