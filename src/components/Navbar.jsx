"use client";

import React, { useEffect } from "react";
import { Navbar, IconButton, Collapse } from "@material-tailwind/react";
import { Bars2Icon } from "@heroicons/react/24/outline";
import Image from "next/image";
import ProfileMenu from "./Navbar/ProfileMenu";
import NavList from "./Navbar/NavLis";
import Link from "next/link";

export function ComplexNavbar() {
  const [isNavOpen, setIsNavOpen] = React.useState(false);

  const toggleIsNavOpen = () => setIsNavOpen((cur) => !cur);

  useEffect(() => {
    window.addEventListener(
      "resize",
      () => window.innerWidth >= 960 && setIsNavOpen(false)
    );
  }, []);

  return (
    <header className={` sticky top-0 z-50`}>
      <Navbar className="mx-auto max-w-screen-xl p-2 lg:rounded-full lg:pl-6">
        <div className="relative mx-auto flex items-center text-blue-gray-900">
          <Link className="flex items-center" href="/">
            <img
              src="./images/logoOficial.png"
              alt="logo"
              className="w-32"
            />
            {/* <img
              className="bg-black w-28"
              src="./images/LogoBarrios.jpg"
              alt="logo"
            /> */}
          </Link>

          <div className="absolute top-2/4 left-2/4 hidden -translate-x-2/4 -translate-y-2/4 lg:block">
            <NavList />
          </div>
          <IconButton
            size="sm"
            color="blue-gray"
            variant="text"
            onClick={toggleIsNavOpen}
            className="ml-auto mr-2 lg:hidden"
          >
            <Bars2Icon className="h-6 w-6" />
          </IconButton>
          <ProfileMenu />
        </div>
        <Collapse open={isNavOpen} className="overflow-scroll">
          <NavList />
        </Collapse>
      </Navbar>
    </header>
  );
}
