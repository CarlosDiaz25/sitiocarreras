"use client";

import { getPatrocinadores } from "@/services/patrocinadores";
import { Card, CardBody, Typography } from "@material-tailwind/react";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/free-mode";

import { FreeMode, Pagination } from "swiper/modules";

const Patrocinadores = ({ idcarrera }) => {
  const [patrocinadores, setPatrocinadores] = useState([]);

  useEffect(() => {
    obtenerPatrocinadores();
  }, []);

  const obtenerPatrocinadores = async () => {
    try {
      const response = await getPatrocinadores(idcarrera);

      if (response.resultado) setPatrocinadores(response.data);
    } catch (error) {
      console.log(error);
      NotificationCustom({
        type: "error",
        message: "Ocurrió un error al obtener los patrocinadores",
      });
    }
  };

  return (
    <section className="text-gray-600 ">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-wrap w-full  flex-col items-center ">
        <Typography variant="h3" color="black">
            Patrocinadores
          </Typography>
        </div>
        <div className="flex flex-wrap justify-center items-center">
          <img
            src={"./images/logoOficial.png"}
            alt="card-image"
            className="rounded-lg w-96 mb-5"
          />
        </div>

        <div className="flex justify-center mb-20">
          <p className="lg:w-1/2 w-full leading-relaxed text-gray-500">
            ¡Expresamos nuestro más sincero agradecimiento a nuestros generosos
            patrocinadores!
          </p>
        </div>

        <Swiper
          breakpoints={{
            340: {
              slidesPerView: 1,
              spaceBetween: 8,
            },
            700: {
              slidesPerView: 3,
              spaceBetween: 15,
            },
          }}
          freeMode={true}
          pagination={{
            clickable: true,
          }}
          modules={[FreeMode, Pagination]}
          className="max-w-[90%] lg:max-w-[80%]"
        >
          {patrocinadores.map(({ rutaImagen, agradecimiento }, index) => (
            <SwiperSlide key={index}>
              <Card className="w-full">
                <CardBody>
                  <div>
                    <img
                      src={rutaImagen}
                      alt="card-image"
                      className="rounded-lg"
                    />
                  </div>
                </CardBody>
              </Card>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </section>
  );
};

export default Patrocinadores;
