import React, { useEffect, useState } from "react";
import { NotificationCustom } from "./ui/NotificationCustom";
import { List, ListItem, Card, Typography } from "@material-tailwind/react";
import { format } from "@/utils/currency";
import { getPremiacionEspecial } from "@/services/premiacionEspecial";

export const PremiacionEspecial = ({ idTipoCarrera }) => {
  const [premiosEspeciales, setPremiosEspeciales] = useState([]);

  useEffect(() => {
    obtenerPremiosEspeciales();
  }, []);

  const obtenerPremiosEspeciales = async () => {
    try {
      const response = await getPremiacionEspecial(idTipoCarrera);

      if (response.resultado) setPremiosEspeciales(response.data);
    } catch (error) {
      console.log(error);
      NotificationCustom({
        type: "error",
        message: "Ocurrió un error al obtener las premiaciones especiales",
      });
    }
  };

  return (
    <div>
      <Card>
        <List>
          {premiosEspeciales.map(
            ({ titulo, premiacionEspecialDetalles }, index) => (
              <ListItem key={index}>
                <div>
                  <Typography variant="h5" color="red">
                    {titulo}
                  </Typography>
                  {premiacionEspecialDetalles.map(
                    ({ descripcion, premio, esDinero }) => (
                      <>
                        <Typography
                          variant="small"
                          color="gray"
                          className="font-normal"
                        >
                          {descripcion}
                        </Typography>
                        <Typography variant="h4" className="flex">
                          {esDinero === true ? format(premio) : premio}
                        </Typography>
                      </>
                    )
                  )}
                </div>
              </ListItem>
            )
          )}
        </List>
      </Card>
    </div>
  );
};
