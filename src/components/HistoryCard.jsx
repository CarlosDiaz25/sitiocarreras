import { Carousel, Typography } from "@material-tailwind/react";
import React from "react";

export const HistoryCard = () => {
  return (
    <div className="flex flex-col items-center mt-20">
         <Typography
          variant="h2"
          className="mb-4 mt-10 "
        >
          Acontecimientos importantes
        </Typography>
      <Carousel className="rounded-xl h-[500px] ">
        <div className="relative h-full w-full">
          <img
            src={"./images/history/History1.png"}
            alt="image 1"
            className="h-full w-full object-cover"
          />
        </div>
        <div className="relative h-full w-full">
          <img
            src={"./images/history/History2.png"}
            alt="image 1"
            className="h-full w-full object-cover"
          />
          
        </div>
        <div className="relative h-full w-full">
          <img
            src={"./images/history/History3.png"}
            alt="image 1"
            className="h-full w-full object-cover"
          />
         
        </div>
        <div className="relative h-full w-full">
          <img
            src={"./images/history/History4.png"}
            alt="image 1"
            className="h-full w-full object-cover"
          />
        
        </div>
      </Carousel>
    </div>
  );
};
