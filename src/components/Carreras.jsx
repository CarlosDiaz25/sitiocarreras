"use client";

import React, { useState } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Typography,
  Button,
  Collapse,
} from "@material-tailwind/react";
import Link from "next/link";
import {
  CheckBadgeIcon,
  CurrencyDollarIcon,
  CalendarIcon,
  TrophyIcon,
} from "@heroicons/react/24/solid";
import { Categorias21KM } from "./TipoCarreras/Categorias21KM";
import { CategoriaInfantil } from "./TipoCarreras/CategoriaInfantil";
import { Categorias10KM } from "./TipoCarreras/Categorias10KM";
import { PremiosEspeciales } from "./TipoCarreras/PremiosEspeciales";

const Carreras = () => {
  const [open, setOpen] = useState(false);
  const [open10km, setOpen10km] = useState(false);
  const [openPremiosEspeciales, setPremiosEspeciales] = useState(false);


  const toggleOpen = () => setOpen((cur) => !cur);
  const toggleOpen10km = () => setOpen10km((cur) => !cur);
  const togglePremiosEspeciales = () => setPremiosEspeciales((cur) => !cur);


  const [openInfantil, setOpenInfantil] = useState(false);
  const toggleOpenInfantil = () => setOpenInfantil((cur) => !cur);
  return (
    <section className="text-gray-600 body-font">
      <div className="container px-5 py-24 mx-auto">
        <div className="flex flex-wrap w-full mb-5 flex-col items-center text-center">
          <Typography
            variant="h1"
            color="black"
            className="mb-4 text-3xl md:text-4xl lg:text-5xl"
          >
            CATEGORÍAS
          </Typography>
        </div>
        <div className="flex flex-wrap -m-4">
          <div className="xl:w-1/3 md:w-1/2 p-4">
            <Card className="mt-6">
              <CardHeader color="blue-gray" className="relative h-50">
                <img src={"./images/carrera10km.jpg"} alt="card-image" />
              </CardHeader>
              <CardBody>
                <Typography variant="h5" color="blue-gray" className="mb-2">
                  Carrera 10 Kilometros
                </Typography>
                <Typography variant="h6" color="blue" className="mb-2">
                  Costo $550 MXN
                </Typography>
                <Typography
                  className="flex mb-2 gap-2 items-center"
                  variant="h6"
                  color="red"
                >
                  <CurrencyDollarIcon className="w-6 h-6" />
                  Carrera con premiación
                </Typography>
                <Typography>
                  Estás invitado/a a formar parte de una experiencia única llena
                  de energía y vitalidad: nuestra emocionante Carrera de 10
                  kilómetros. Si eres un entusiasta del running, un amante de
                  los desafíos o simplemente deseas disfrutar de un día activo y
                  enérgico, ¡esta es tu oportunidad!
                </Typography>
              </CardBody>
              <CardFooter className="pt-0">
                <div className="flex justify-between">
                  <Link href="/boleto" passHref>
                    <Button size="lg" className="bg-[#0000de] ">
                      Inscribirme
                    </Button>
                  </Link>
                  <Button
                    onClick={toggleOpen10km}
                    className="flex gap-2 justify-center items-center"
                    color="red"
                  >
                    <CheckBadgeIcon className="w-6" />
                    Premios..
                  </Button>
                </div>
                <Collapse open={open10km}>
                  <Categorias10KM />
                </Collapse>
              </CardFooter>
            </Card>
          </div>
          <div className="xl:w-1/3 md:w-1/2  p-4">
            <Card className="mt-6">
              <CardHeader color="blue-gray" className="relative h-50">
                <img src={"./images/carrera21km.jpg"} alt="carrera21km" />
              </CardHeader>
              <CardBody>
                <Typography variant="h5" color="blue-gray" className="mb-2">
                  Carrera 21 Kilometros
                </Typography>
                <Typography variant="h6" color="blue" className="mb-2">
                  Costo $550
                </Typography>
                <Typography
                  className="flex mb-2 gap-2 items-center"
                  variant="h6"
                  color="red"
                >
                  <CurrencyDollarIcon className="w-6 h-6" />
                  Carrera con premiación y premios especiales
                </Typography>
                <Typography>
                  Esta carrera es mucho más que un simple recorrido de 21
                  kilómetros. Es una oportunidad para medir tu resistencia,
                  fortaleza y determinación mientras recorres un desafiante
                  trayecto diseñado para inspirarte y motivarte. Cada paso que
                  des te acercará a la satisfacción de cruzar la línea de meta,
                  demostrando que puedes lograr lo que te propongas.
                </Typography>
              </CardBody>
              <CardFooter className="pt-0">
                <div className="flex justify-between">
                  <Link href="/boleto" passHref>
                    <Button size="lg" className="bg-[#0000de] ">
                      Inscribirme
                    </Button>
                  </Link>
                  <Button
                    onClick={toggleOpen}
                    className="flex gap-2 justify-center items-center"
                    color="red"
                  >
                    <CheckBadgeIcon className="w-6" />
                    Premios..
                  </Button>
                </div>
                <div className="mt-5 flex items-center justify-center">
                  <Button
                    onClick={togglePremiosEspeciales}
                    className="flex gap-2 justify-center items-center"
                    color="red"
                  >
                    <TrophyIcon className="w-6" />
                    Premios Especiales..
                  </Button>
                </div>

                <Collapse open={open}>
                  <Categorias21KM />
                </Collapse>
                <Collapse open={openPremiosEspeciales}>
                  <PremiosEspeciales />
                </Collapse>
              </CardFooter>
            </Card>
          </div>
          <div className="xl:w-1/3 md:w-1/2 p-4">
            <Card className="mt-6">
              <CardHeader color="blue-gray" className="relative h-50">
                <img src={"./images/carrerainfantil.jpg"} alt="carrerainfantil" />
              </CardHeader>
              <CardBody>
                <Typography variant="h5" color="blue-gray" className="mb-2">
                  Carrera Infantil
                </Typography>
                <Typography variant="h6" color="blue" className="mb-2">
                  Costo $100
                </Typography>
                <Typography
                  className="flex mb-2 gap-2 items-center"
                  variant="h6"
                  color="red"
                >
                  <CalendarIcon className="w-6 h-6" />
                  Edades de 2 a 14 Años
                </Typography>
                <Typography>
                  ¡Niños y niñas de todas las edades están invitados a un día
                  lleno de diversión, risas y movimiento en nuestra Carrera
                  Infantil Divertilandia! Si buscas una manera emocionante de
                  pasar tiempo activo en familia y amigos, este evento es
                  perfecto para ti.
                </Typography>
              </CardBody>
              <CardFooter className="pt-0">
                <div className="flex  justify-between">
                  <Link href="/boleto" passHref>
                    <Button size="lg" className="bg-[#0000de] ">
                      Inscribirme
                    </Button>
                  </Link>
                  <Button onClick={toggleOpenInfantil} color="red">
                    Saber mas..
                  </Button>
                </div>
                <Collapse open={openInfantil}>
                  <CategoriaInfantil />
                </Collapse>
              </CardFooter>
            </Card>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Carreras;
