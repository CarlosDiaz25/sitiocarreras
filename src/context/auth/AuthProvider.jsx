import { useEffect, useReducer } from "react";
import { AuthContext, authReducer } from ".";
import { login } from "@/services/usuario";
import Cookies from "js-cookie";
import { validateToken } from "@/services/token";
import { useRouter } from "next/navigation";
import { useSession,signOut } from "next-auth/react";

const initialState = {
  isLoggedIn: false,
  user: undefined,
};

export const AuthProvider = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, initialState);
  const {data, status } = useSession();
  const router = useRouter();

  useEffect(() => {
    if( status === 'authenticated'){
      dispatch({ type: "[Auth] - Login", payload: data.user });
    }
  }, [ status, data ])
  
  // useEffect(() => {
  //   checkToken();
  // }, []);

  // const checkToken = async () => {
  //   try {
  //     if (!Cookies.get("token")) return;

  //     const peticion = await validateToken(Cookies.get("token"));
  //     if (peticion.resultado) {
  //       Cookies.set("token", peticion.token);
  //       dispatch({ type: "[Auth] - Login", payload: peticion.data });
  //       return true;
  //     }
  //     Cookies.remove("token");
  //   } catch (error) {
  //     Cookies.remove("token");
  //   }
  // };

  // const loginUser = async (email, password) => {
  //   try {
  //     const usuario = await login({ correo: email, contrasenia: password });

  //     if (usuario.resultado) {
  //       Cookies.set("token", usuario.token);
  //       dispatch({ type: "[Auth] - Login", payload: usuario.data });
  //       return true;
  //     }

  //     return false;
  //   } catch (error) {
  //     return false;
  //   }
  // };

  const logout = () => {
    Cookies.remove('token');
    Cookies.remove('cart');
   
    signOut({ callbackUrl: '/' });
  }

  return (
    <AuthContext.Provider
      value={{
        ...state,
        logout,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
