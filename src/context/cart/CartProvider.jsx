import { useEffect, useReducer } from "react";
import { CartContext, cartReducer } from "./";
import Cookie from "js-cookie";

const initialState = {
  isLoaded: false,
  cart: [],
  numberOfItems: 0,
  subTotal: 0,
  costoServicio: 0,
  total: 0,
  comision: 0,
  descuentoAplicado:0
};

export const CartProvider = ({ children }) => {
  const [state, dispatch] = useReducer(cartReducer, initialState);

  useEffect(() => {
    try {
      const cookieFichas = Cookie.get("cart")
        ? JSON.parse(Cookie.get("cart"))
        : [];
      dispatch({
        type: "[Cart] - Loadcart from cookies | storage",
        payload: cookieFichas,
      });
    } catch (error) {
      dispatch({
        type: "[Cart] - Loadcart from cookies | storage",
        payload: [],
      });
    }
  }, []);

  useEffect(() => {
    Cookie.set("cart", JSON.stringify(state.cart));
  }, [state.cart]);

  useEffect(() => {
    calcularCosto();
  }, [state.comision]);

  useEffect(() => {
    calcularCosto();
  }, [state.cart]);

  const addFichastoCart = (ficha) => {
    const existeParticipante = state.cart.some(
      (p) => p.participante.id === ficha.participante.id && p.carrera.id === ficha.carrera.id
    );
    if (existeParticipante)
      return "El participante ya cuenta con un boleto en de esta carrera en el carrito";

    return dispatch({ type: "[Cart] - Update fichas in cart", payload: ficha });
  };

  const removeCartFicha = (ficha) => {
    dispatch({ type: "[Cart] - Remove ficha in cart", payload: ficha });
  };

  const OrderComplete = () => {
    dispatch({ type: "[Cart] - Order complete" });
  };

  const calcularCosto = () => {
    const numberOfItems = state.cart.reduce((prev) => 1 + prev, 0);
    const subTotal = state.cart.reduce((prev, current) => current.subcategoria.costo + prev,0);
    const descuentoAplicado = state.cart.reduce((prev, current) => current.carrera.descuentoAplicado + prev,0);
    const costoServicio = redondearDecimales(state.cart.reduce((prev, current) => ((current.subcategoria.costo - current.carrera.descuentoAplicado) * (current.carrera.comision / 100))  + prev,0),2);
    
    const orderSummary = {
      numberOfItems,
      subTotal,
      costoServicio,
      total: ((subTotal - descuentoAplicado) + costoServicio),
      descuentoAplicado:descuentoAplicado
    };

    dispatch({
      type: "[Cart] - Update order summay",
      payload: orderSummary,
    });
  };

  function redondearDecimales(numero, decimales) {
    const numeroRegexp = new RegExp('\\d\\.(\\d){' + decimales + ',}');   // Expresion regular para numeros con un cierto numero de decimales o mas
    if (numeroRegexp.test(numero)) {         // Ya que el numero tiene el numero de decimales requeridos o mas, se realiza el redondeo
        return Number(numero.toFixed(decimales));
    } else {
        return Number(numero.toFixed(decimales)) === 0 ? 0 : numero;  // En valores muy bajos, se comprueba si el numero es 0 (con el redondeo deseado), si no lo es se devuelve el numero otra vez.
    }
}

  return (
    <CartContext.Provider
      value={{
        ...state,
        addFichastoCart,
        removeCartFicha,
        OrderComplete,
      }}
    >
      {children}
    </CartContext.Provider>
  );
};
