export const cartReducer = (state, action) => {
  switch (action.type) {
    case "[Cart] - Loadcart from cookies | storage":
      return {
        ...state,
        isLoaded: true,
        cart: [...action.payload],
      };

    case "[Cart] - Update fichas in cart":
      return {
        ...state,
        cart: [...state.cart, action.payload],
      };

    case "[Cart] - Remove ficha in cart":
      return {
        ...state,
        cart: [
          ...state.cart.filter(
            (ficha) =>
              !(
                ficha.participante.id === action.payload.participante.id &&
                ficha.carrera.id === action.payload.carrera.id
              )
          ),
        ],
      };

    case "[Cart] - Update order summay":
      return {
        ...state,
        ...action.payload,
      };
    case "[Cart] - Update info carrera":
      return {
        ...state,
        comision:action.payload.comision,
        descuento:action.payload.descuento,
      };
    case "[Cart] - Order complete":
      return {
        ...state,
        cart: [],
        numberOfItems: 0,
        subTotal: 0,
        costoServicio: 0,
        total: 0,
        descuentoAplicado:0
      };
    default:
      return state;
  }
};
