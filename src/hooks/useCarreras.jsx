import useSWR from 'swr';

export const useCarreras = (url,config = {}) =>{

    const { data, error } = useSWR(`${process.env.NEXT_PUBLIC_API_URL}/carrera${ url }`,config);

    return {
        carrera: data,
        isLoading: !error && !data,
        isError: error
    }
}