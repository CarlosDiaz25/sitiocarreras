import axios from "axios";

const BASE_URL = `${process.env.NEXT_PUBLIC_API_LOCAL_URL}/mercadopago`;

export const createOrder = async (data) => {
    try {
        const response = await axios.post(`${BASE_URL}`, data);
        return response;
    } catch (error) {
        return error.data;
    }
};
