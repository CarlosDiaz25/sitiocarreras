import axios from "axios";

const BASE_URL = `${process.env.NEXT_PUBLIC_API_URL}/carrera`;

/**
 * 
 * @param {object} data Carreras data
 * @returns Promise
 */
export const getCarreras = async () => {
    try {
        const response = await axios.get(`${BASE_URL}/obtener`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

/**
 * 
 * @param {object} data Carreras data
 * @returns Promise
 */
export const getInfoCarrera = async (idCarrera) => {
    try {
        const response = await axios.get(`${BASE_URL}/obtener/${idCarrera}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

/**
 * 
 * @param {object} data Carrousel data
 * @returns Promise
 */
export const getCarrousel = async () => {
    try {
        const response = await axios.get(`${BASE_URL}/getCarrousel`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

/**
 * 
 * @param {object} data Carrousel for career data
 * @returns Promise
 */
export const getCarrouselForCareer= async (idCarrera) => {
    try {
        const response = await axios.get(`${BASE_URL}/getCarrouselForCareer/${idCarrera}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

/**
 * 
 * @param {object} data Carrousel for career data
 * @returns Promise
 */
export const getDetailCareer= async (idCarrera) => {
    try {
        const response = await axios.get(`${BASE_URL}/getDetallesCarrera/${idCarrera}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

