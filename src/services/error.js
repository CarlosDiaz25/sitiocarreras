import axios from "axios";

const BASE_URL = `${process.env.NEXT_PUBLIC_API_URL}/erroresMercadoPago`;

/**
 * login
 * @param {object} data Participante data
 * @returns Promise
 */
export const saveErrorMercadoPago = async (data) => {
    try {
        const response = await axios.post(`${BASE_URL}/agregar`, data);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};