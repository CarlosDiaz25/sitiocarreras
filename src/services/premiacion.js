import axios from "axios";

const BASE_URL = `${process.env.NEXT_PUBLIC_API_URL}/premiaciones`;

/**
 * 
 * @param {object} data Carreras data
 * @returns Promise
 */
export const getReward = async (idTipoCarrera) => {
    try {
        const response = await axios.get(`${BASE_URL}/obtener/${idTipoCarrera}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};