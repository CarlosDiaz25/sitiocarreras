import axios from "axios";

const BASE_URL = `${process.env.NEXT_PUBLIC_API_URL}/solicituContacto`;

/**
 * 
 * @param {object} data Carreras data
 * @returns Promise
 */
export const seedMessage = async (data) => {
    try {
        const response = await axios.post(`${BASE_URL}/contact`, data);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};