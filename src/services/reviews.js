import axios from "axios";

const BASE_URL = `${process.env.NEXT_PUBLIC_API_URL}/opiniones`;

/**
 * 
 * @param {object} data Carreras data
 * @returns Promise
 */
export const getReviews = async () => {
    try {
        const response = await axios.get(`${BASE_URL}/getReviews`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};