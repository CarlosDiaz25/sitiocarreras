import axios from "axios";

const BASE_URL = `${process.env.NEXT_PUBLIC_API_URL}/compras`;

/**
 * login
 * @param {object} data Participante data
 * @returns Promise
 */
export const saveCompra = async (data) => {
    try {
        const response = await axios.post(`${BASE_URL}/agregar`, data);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};


export const validarCompra = async (data) => {
    try {
        const response = await axios.post(`${BASE_URL}/validarUsuarioCarrera`, data);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

export const validarCompraGuardada = async (orderId,formaPago) => {
    try {
        const response = await axios.get(`${BASE_URL}/ExisteCompra?pagoId=${orderId}&&formaPago=${formaPago}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

export const obtenerCompras = async (usuarioId) => {
    try {
        const response = await axios.get(`${BASE_URL}/obtenerComprasByIdUsuario?IdUsuario=${usuarioId}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

export const obtenerDetalleCompras = async (detalleId) => {
    try {
        const response = await axios.get(`${BASE_URL}/ObtenerDetallByIdCompra?IdCompra=${detalleId}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

export const verficarEstatus = async (data) => {
    try {
        const response = await axios.post(`${BASE_URL}/verificarEstatus`,data);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

export const validarCompraPrePago = async ( data ) => {
    try {
        const response = await axios.post(`${BASE_URL}/validarCompraPrePago`, data);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
}

export const existeCompra = async (pagoId) => {
    try {
        const response = await axios.get(`${BASE_URL}/existeCompra?pagoId=${pagoId}&formaPago=1`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

export const renviarCorreoCompra = async (IdCompra) => {
    try {
        const response = await axios.get(`${BASE_URL}/enviarCorreo?id=${IdCompra}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};