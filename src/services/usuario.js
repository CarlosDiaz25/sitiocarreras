import axios from "axios";

const BASE_URL = `${process.env.NEXT_PUBLIC_API_URL}/usuarios`;

/**
 * login
 * @param {object} data Usuario data
 * @returns Promise
 */
export const login = async (data) => {
    try {
        const response = await axios.post(`${BASE_URL}/login`, data);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

/**
 * Save new Usuario
 * @param {object} data Usuario data
 * @returns Promise
 */
export const saveUsuario = async (data) => {
    try {
        const response = await axios.post(`${BASE_URL}/store`, data);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

/**
 * Save new Usuario
 * @param {object} data Usuario data
 * @returns Promise
 */
export const updateUsuario = async (data) => {
    try {
        const response = await axios.post(`${BASE_URL}/update`, data);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

export const cambiarContrasenia = async (data) => {
    try {
        const response = await axios.post(`${BASE_URL}/actualizarContrasenia`, data);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};

export const recuperarContrasenia = async (correo) => {
    try {
        const response = await axios.get(`${BASE_URL}/recuperar?correo=${correo}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};