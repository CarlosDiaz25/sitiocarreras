import axios from "axios";

const BASE_URL = `${process.env.NEXT_PUBLIC_API_URL}/tallas`;

export const getTallas = async (idCarrera) => {
    try {
        const response = await axios.get(`${BASE_URL}/obtener/${idCarrera}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};