import axios from "axios";

const BASE_URL = `${process.env.NEXT_PUBLIC_API_URL}/token`;

/**
 * validar token
 * @param {object} Token data
 * @returns Promise
 */
export const validateToken = async (token) => {
    try {
        const response = await axios.get(`${BASE_URL}/validate-token?token=${token}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};