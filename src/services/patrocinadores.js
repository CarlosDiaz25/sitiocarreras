import axios from "axios";

const BASE_URL = `${process.env.NEXT_PUBLIC_API_URL}/patrocinador`;

/**
 * login
 * @param {object} data Participante data
 * @returns Promise
 */
export const getPatrocinadores = async (idcarrera) => {
    try {
        const response = await axios.get(`${BASE_URL}/getPatrocinador/${idcarrera}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};