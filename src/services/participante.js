import axios from "axios";

const BASE_URL = `${process.env.NEXT_PUBLIC_API_URL}/participantes`;

/**
 * login
 * @param {object} data Participante data
 * @returns Promise
 */
export const saveParticipante = async (data) => {
    try {
        const response = await axios.post(`${BASE_URL}/agregar`, data);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};


export const getParticipante = async (usuarioId) => {
    try {
        const response = await axios.get(`${BASE_URL}/obtenerByUsuarioId?idUsuario=${usuarioId}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};


export const getSubCategorias = async (participanteId,idTipoCarrera) => {
    try {
        const response = await axios.get(`${BASE_URL}/ObtenerCategoriaTipoCarreraByParticipante?id=${participanteId}&idTipoCarrera=${idTipoCarrera}`);
        return response.data;
    } catch (error) {
        return error.response.data;
    }
};