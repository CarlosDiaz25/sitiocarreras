"use client";

import {
  obtenerCompras,
  obtenerDetalleCompras,
  renviarCorreoCompra,
  verficarEstatus,
} from "@/services/compras";
import {
  ArrowPathIcon,
  EnvelopeIcon,
  EyeIcon,
} from "@heroicons/react/24/solid";
import {
  Card,
  CardHeader,
  Typography,
  Button,
  CardBody,
  Chip,
  Avatar,
  IconButton,
  Tooltip,
  Dialog,
  DialogHeader,
  DialogBody,
  DialogFooter,
} from "@material-tailwind/react";
import { useSession } from "next-auth/react";
import { useEffect, useState } from "react";
import { currency } from "../../utils";
import { FullScreenLoading } from "@/components/ui/FullScreenLoading";
import { NotificationCustom } from "@/components/ui/NotificationCustom";

const TABLE_HEAD = [
  "No. orden",
  "No. fichas",
  "Total",
  "Total pagado",
  "Tarjeta",
  "Fecha",
  "Estatus",
  "Forma de pago",
  "",
];

const TABLE_HEAD_DETALLE = [
  "Carrera",
  "Participante",
  "Talla",
  "Categoria",
  "SubCategoria",
  "Hora evento",
  "Boleto",
];

export default function TransactionsTable() {
  const { data: session, status } = useSession();
  const [ordenes, setOrdenes] = useState([]);
  const [ordenDetalle, setOrdenDetalle] = useState([]);
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [loadingDetails, setLoadingDetails] = useState(false);

  useEffect(() => {
    if (status === "authenticated") {
      obtenerOrdenes(session.user.id);
    }
  }, [status]);

  const obtenerOrdenes = async (usuarioId) => {
    try {
      setLoading(true);
      const ordenesResponse = await obtenerCompras(usuarioId);
      if (ordenesResponse.resultado) setOrdenes(ordenesResponse.data);
    } catch (error) {
      NotificationCustom({
        type: "error",
        message: "Ocurrió un error al obtener las compras"
      });
    } finally {
      setLoading(false);
    }
  };

  const obtenerDetallesOrden = async (detalleId) => {
    try {
      setLoadingDetails(true);

      const ordenDetalleResponse = await obtenerDetalleCompras(detalleId);
      
      if (ordenDetalleResponse.resultado) {
        setOrdenDetalle(ordenDetalleResponse.data);
      } else {
        NotificationCustom({
          type: "info",
          message: ordenDetalleResponse.data
        });
      }

    } catch (error) {

     NotificationCustom({
        type: "info",
        message: "Ocurrió un error al obtener el detalle de la compra"
      });

    } finally {
      setLoadingDetails(false);
    }
  };

  const validarEstatus = async (compraId) => {
    try {
      setLoading(true);

      const statusPeticion = await verficarEstatus({ idCompra: compraId });

      if (statusPeticion.resultado) {
        NotificationCustom({
          type: "success",
          message: "Se verificó correctamente la compra"
        });

        await obtenerOrdenes(session.user.id);

      } else {
       NotificationCustom({
          type: "info",
          message: statusPeticion.data
        });
      }
    } catch (error) {
      NotificationCustom({
        type: "info",
        message: "Ocurrió un error al obtener el detalle de la compra"
      });
    } finally {
      setLoading(false);
    }
  };

  const handleOpen = async (id) => {
    if (!open) await obtenerDetallesOrden(id);
    setOpen(!open);
  };

  const handleValidarEstatus = async (id) => {
    await validarEstatus(id);
  };

  const handleSeendEmail = async (id) => {
    const statusPeticion = await renviarCorreoCompra(id);
    if(statusPeticion.resultado){
      NotificationCustom({
        type:'success',
        message:'Correo enviado con exito'
      });
    }
  };

  return (
    <Card className="max-h-[calc(100vh-8rem)] w-full">
      <CardHeader floated={false} shadow={false} className="rounded-none">
        <div className="mb-4 flex flex-col justify-between gap-8 md:flex-row md:items-center">
          <div>
            <Typography variant="h5" color="blue-gray">
              Compras realizadas
            </Typography>
            <Typography color="gray" className=" font-normal">
              Aquí se muestra el detalle de las compras realizadas
            </Typography>
          </div>
        </div>
        {loading ? <FullScreenLoading /> : <></>}
      </CardHeader>
      <CardBody className="overflow-scroll px-0">
        {ordenes.length === 0 ? (
          <Chip
            variant="ghost"
            color="blue"
            size="sm"
            value="No ha realizado ninguna compra"
            className="mb-2"
            icon={
              <span className="mx-auto mt-1 block h-2 w-2 rounded-full bg-blue-900 content-['']" />
            }
          />
        ) : (
          <></>
        )}

        <table className="w-full table-auto text-left">
          <thead>
            <tr>
              {TABLE_HEAD.map((head) => (
                <th
                  key={head}
                  className="border-y border-blue-gray-100 bg-blue-gray-50/50 p-4"
                >
                  <Typography
                    variant="small"
                    color="blue-gray"
                    className="font-normal leading-none opacity-70"
                  >
                    {head}
                  </Typography>
                </th>
              ))}
            </tr>
          </thead>
          <tbody>
            {ordenes.map(
              (
                {
                  id,
                  cantidadBoletos,
                  total,
                  totalPagado,
                  tarjeta,
                  fechaCompra,
                  estatus,
                  formaDePago,
                },
                index
              ) => {
                const isLast = index === ordenes.length - 1;
                const classes = isLast
                  ? "p-4"
                  : "p-4 border-b border-blue-gray-50";

                return (
                  <tr key={id}>
                    <td className={classes}>
                      <div className="flex items-center gap-3">
                        <Typography
                          variant="small"
                          color="blue-gray"
                          className="font-bold"
                        >
                          {id}
                        </Typography>
                      </div>
                    </td>
                    <td className={classes}>
                      <Typography
                        variant="small"
                        color="blue-gray"
                        className="font-normal"
                      >
                        {cantidadBoletos}
                      </Typography>
                    </td>
                    <td className={classes}>
                      <Typography
                        variant="small"
                        color="blue-gray"
                        className="font-normal"
                      >
                        {currency.format(total)}
                      </Typography>
                    </td>
                    <td className={classes}>
                      <Typography
                        variant="small"
                        color="blue-gray"
                        className="font-normal"
                      >
                        {currency.format(totalPagado)}
                      </Typography>
                    </td>
                    <td className={classes}>
                      <Typography
                        variant="small"
                        color="blue-gray"
                        className="font-normal"
                      >
                        {tarjeta}
                      </Typography>
                    </td>
                    <td className={classes}>
                      <Typography
                        variant="small"
                        color="blue-gray"
                        className="font-normal"
                      >
                        {fechaCompra}
                      </Typography>
                    </td>
                    <td className={classes}>
                      <div className="w-max">
                        <Chip
                          size="sm"
                          variant="ghost"
                          value={estatus}
                          color={
                            estatus === "PAGADO"
                              ? "green"
                              : estatus === "PENDIENTE"
                              ? "amber"
                              : estatus === "EN PROCESO"
                              ? "brown"
                              : "red"
                          }
                        />
                      </div>
                    </td>
                    <td className={classes}>
                      <div className="flex items-center gap-3">
                        <div className="h-12 w-12 rounded-md border border-blue-gray-50 p-1">
                          <Avatar
                            src={
                              formaDePago === "Mercado Pago"
                                ? "./iconos/MercadoPago.png"
                                : "./iconos/paypal.png"
                            }
                            size="sm"
                            alt={"account"}
                            variant="square"
                            className="h-full w-full object-contain p-1"
                          />
                        </div>
                        <div className="flex flex-col">
                          <Typography
                            variant="small"
                            color="blue-gray"
                            className="font-normal capitalize"
                          >
                            {formaDePago}
                          </Typography>
                        </div>
                      </div>
                    </td>
                    <td className={classes}>
                      <div className="flex justify-between gap-2">
                        <Tooltip content="Ver Detalles">
                          <IconButton
                            onClick={() => handleOpen(id)}
                            className="flex items-center gap-3 bg-[#0000de] "
                          >
                            <EyeIcon className="h-4 w-4 " />
                          </IconButton>
                        </Tooltip>
                        {formaDePago === "Mercado Pago" &&
                        estatus !== "PAGADO" &&
                        estatus !== "CANCELADO" ? (
                          <Tooltip content="Verificar Estatus">
                            <IconButton
                              onClick={() => handleValidarEstatus(id)}
                              color="red"
                            >
                              <ArrowPathIcon id={id} className="h-6 w-6" />
                            </IconButton>
                          </Tooltip>
                        ) : (
                          <></>
                        )}
                        {estatus === "PAGADO" && (
                          <Tooltip content="Re-enviar correo">
                            <IconButton
                              onClick={() => {
                                handleSeendEmail(id);
                              }}
                              color="yellow"
                              className="flex items-center gap-3"
                            >
                              <EnvelopeIcon className="h-4 w-4 " />
                            </IconButton>
                          </Tooltip>
                        )}
                      </div>
                    </td>
                  </tr>
                );
              }
            )}
          </tbody>
        </table>
      </CardBody>
      <Dialog size={"lg"} handler={handleOpen} open={open}>
        <DialogHeader>Detalle de la compra.</DialogHeader>
        <DialogBody divider>
          <Card className="max-h-[calc(100vh-8rem)] w-full">
            <CardHeader floated={false} shadow={false} className="rounded-none">
              <div className="flex flex-col justify-between gap-8 md:flex-row md:items-center">
                <div>
                  <Typography variant="h5" color="blue-gray">
                    Fichas obtenidas
                  </Typography>
                  <Typography color="gray" className=" font-normal">
                    Aquí se muestra el detalle de la compra realizada
                  </Typography>
                  <Chip
                    variant="ghost"
                    color="red"
                    size="sm"
                    value="NOTA... Si su compra no está pagada su boleto no tendrá valides alguno"
                    className="mb-2"
                    icon={
                      <span className="mx-auto mt-1 block h-2 w-2 rounded-full bg-red-900 content-['']" />
                    }
                  />
                </div>
              </div>
              {loadingDetails ? <FullScreenLoading /> : <></>}
            </CardHeader>
            <CardBody className="overflow-scroll px-0">
              <table className="w-full table-auto text-left">
                <thead>
                  <tr>
                    {TABLE_HEAD_DETALLE.map((head) => (
                      <th
                        key={head}
                        className="border-y border-blue-gray-100 bg-blue-gray-50/50 p-4"
                      >
                        <Typography
                          variant="small"
                          color="blue-gray"
                          className="font-normal leading-none opacity-70"
                        >
                          {head}
                        </Typography>
                      </th>
                    ))}
                  </tr>
                </thead>
                <tbody>
                  {ordenDetalle.map(
                    (
                      {
                        id,
                        carrera,
                        nombreParticipante,
                        nombreTalla,
                        nombreTipoCarrera,
                        subCategoria,
                        horaEvento,
                        claveBoleto,
                      },
                      index
                    ) => {
                      const isLast = index === ordenes.length - 1;
                      const classes = isLast
                        ? "p-4"
                        : "p-4 border-b border-blue-gray-50";

                      return (
                        <tr key={id}>
                          <td className={classes}>
                            <div className="flex items-center gap-3">
                              <Typography
                                variant="small"
                                color="blue-gray"
                                className="font-bold"
                              >
                                {carrera}
                              </Typography>
                            </div>
                          </td>
                          <td className={classes}>
                            <div className="flex items-center gap-3">
                              <Typography
                                variant="small"
                                color="blue-gray"
                                className="font-bold"
                              >
                                {nombreParticipante}
                              </Typography>
                            </div>
                          </td>
                          <td className={classes}>
                            <Typography
                              variant="small"
                              color="blue-gray"
                              className="font-normal"
                            >
                              {nombreTalla}
                            </Typography>
                          </td>
                          <td className={classes}>
                            <Typography
                              variant="small"
                              color="blue-gray"
                              className="font-normal"
                            >
                              {nombreTipoCarrera}
                            </Typography>
                          </td>
                          <td className={classes}>
                            <Typography
                              variant="small"
                              color="blue-gray"
                              className="font-normal"
                            >
                              {subCategoria}
                            </Typography>
                          </td>
                          <td className={classes}>
                            <Typography
                              variant="small"
                              color="blue-gray"
                              className="font-normal"
                            >
                              {horaEvento}
                            </Typography>
                          </td>
                          <td className={classes}>
                            <Typography
                              variant="small"
                              color="blue-gray"
                              className="font-normal"
                            >
                              {claveBoleto}
                            </Typography>
                          </td>
                        </tr>
                      );
                    }
                  )}
                </tbody>
              </table>
            </CardBody>
          </Card>
        </DialogBody>
        <DialogFooter>
          <Button className="bg-[#0000de] " onClick={() => handleOpen()}>
            <span>Aceptar</span>
          </Button>
        </DialogFooter>
      </Dialog>
    </Card>
  );
}
