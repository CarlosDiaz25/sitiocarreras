"use client";
import ChangePassword from "@/components/account/ChangePassword";
import { useToggle } from "@/hooks/useToggle";
import { updateUsuario } from "@/services/usuario";
import {
  Button,
  Card,
  CardBody,
  Input,
  Option,
  Select,
  Typography,
} from "@material-tailwind/react";
import { useSession } from "next-auth/react";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

function ProfilePage() {
  const modalForm = useToggle();
  const { data: session, status, update } = useSession();
  const MySwal = withReactContent(Swal);
  const [noEditar, setNoEditar] = useState(true);
  const [informacionUsuario, setInformacionUsuario] = useState({
    id: "",
    nombre: "",
    apellidoPaterno: "",
    apellidoMaterno: "",
    telefono: "",
    correoElectronico: "",
    fechaNacimiento: "",
    genero: "",
  });

  const optionsGenero = [
    { id: "MASCULINO", nombre: "MASCULINO" },
    { id: "FEMENINO", nombre: "FEMENINO" },
    { id: "OTRO", nombre: "OTRO" },
  ];

  useEffect(() => {
    if (status === "authenticated") {
      setInformacionUsuario({
        id: session.user.id,
        nombre: session.user.nombre,
        apellidoPaterno: session.user.apellidoPaterno,
        apellidoMaterno: session.user.apellidoMaterno,
        telefono: session.user.telefono,
        correoElectronico: session.user.correo,
        fechaNacimiento: session.user.fechaNacimiento,
        genero: session.user.genero,
      });
    }
  }, [session, status]);

  const handleChangeValueForm = (event) => {
    const { name, value } = event.target;
    setInformacionUsuario({
      ...informacionUsuario,
      [name]: value,
    });
  };

  const handleChangeValueFormSelectGenero = (id) => {
    setInformacionUsuario({
      ...informacionUsuario,
      genero: optionsGenero.find((genero) => genero.id === id).nombre,
    });
  };

  const habilitarComponentes = () => {
    setNoEditar(false);
  };

  const actualizarUsuario = async () => {
    try {
      if (informacionUsuario.nombre === "") {
        MySwal.fire({
          icon: "info",
          text: "Favor de agregar su nombre",
        });
        return false;
      }

      if (informacionUsuario.apellidoPaterno === "") {
        MySwal.fire({
          icon: "info",
          text: "Favor de agregar su Apellido paterno",
        });
        return false;
      }

      if (informacionUsuario.telefono === "") {
        MySwal.fire({
          icon: "info",
          text: "Favor de agregar su teléfono",
        });
        return false;
      }

      if (informacionUsuario.fechaNacimiento === "") {
        MySwal.fire({
          icon: "info",
          text: "Favor de agregar su fecha de nacimiento",
        });
        return false;
      }

      if (informacionUsuario.correoElectronico === "") {
        MySwal.fire({
          icon: "info",
          text: "Favor de agregar su correo electrónico",
        });
        return false;
      }

      var validEmail = /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/;
      if (!validEmail.test(informacionUsuario.correoElectronico)) {
        MySwal.fire({
          icon: "info",
          text: "El Correo electrónico es invalido",
        });
        return false;
      }

      if (informacionUsuario.genero === "") {
        MySwal.fire({
          icon: "info",
          text: "Favor de seleccionar su género",
        });
        return false;
      }

      const updateResponse = await updateUsuario(informacionUsuario);

      if (updateResponse.resultado) {
        await update({
          ...session,
          user: {
            id:informacionUsuario.id,
            nombre: informacionUsuario.nombre,
            apellidoPaterno: informacionUsuario.apellidoPaterno,
            apellidoMaterno: informacionUsuario.apellidoMaterno,
            telefono: informacionUsuario.telefono,
            correo: informacionUsuario.correoElectronico,
            fechaNacimiento: informacionUsuario.fechaNacimiento,
            genero: informacionUsuario.genero,
            token:updateResponse.data.token
          },
        });
        setNoEditar(true);

        await MySwal.fire({
          icon: "success",
          title: "Proceso generado con éxito"
        });

        return;
      }

      MySwal.fire({
        icon: "info",
        text: updateResponse.data,
      });
    } catch (error) {
      console.log(error);
      MySwal.fire({
        icon: "error",
        text: "Ocurrió un error al actualizar la información",
      });
    }
  };

  const handleOpenChangePasssword = () => {
    modalForm.onToggle();
  }

  return (
    <div className="flex justify-center">
      <Card className="w-full max-w-[68rem]">
        <CardBody>
          <Typography variant="h5" color="blue-gray" className="mb-4">
            Información personal
          </Typography>

          <div className="flex flex-wrap gap-5">
            <div className="flex flex-wrap mb-2">
              <Typography variant="h6" color="blue-gray">
                Nombre:
              </Typography>
              <Input
                size="md"
                disabled={noEditar}
                name="nombre"
                value={informacionUsuario.nombre}
                onChange={handleChangeValueForm}
              />
            </div>
            <div className="flex flex-wrap mb-2">
              <Typography variant="h6" color="blue-gray">
                Apellido paterno:
              </Typography>
              <Input
                size="md"
                disabled={noEditar}
                name="apellidoPaterno"
                value={informacionUsuario.apellidoPaterno}
                onChange={handleChangeValueForm}
              />
            </div>
            <div className="flex flex-wrap mb-2">
              <Typography variant="h6" color="blue-gray">
                Apellido materno:
              </Typography>
              <Input
                size="md"
                disabled={noEditar}
                name="apellidoMaterno"
                value={informacionUsuario.apellidoMaterno}
                onChange={handleChangeValueForm}
              />
            </div>
          </div>
          <div className="flex flex-wrap gap-5">
            <div className="flex flex-wrap">
              <Typography variant="h6" color="blue-gray">
                Teléfono:
              </Typography>
              <Input
                size="md"
                disabled={noEditar}
                name="telefono"
                value={informacionUsuario.telefono}
                onChange={handleChangeValueForm}
              />
            </div>

            <div className="flex flex-wrap mr-16">
              <Typography variant="h6" color="blue-gray">
                Generó:
              </Typography>
              <Select
                onChange={handleChangeValueFormSelectGenero}
                disabled={noEditar}
                value={informacionUsuario.genero}
              >
                {optionsGenero.map((option) => (
                  <Option key={option.id} value={option.id}>
                    {option.nombre}
                  </Option>
                ))}
              </Select>
            </div>

            <div className="flex flex-wrap">
              <Typography variant="h6" color="blue-gray">
                Fecha nacimiento:
              </Typography>
              <Input
                size="md"
                type="date"
                disabled={noEditar}
                value={informacionUsuario.fechaNacimiento}
                name="fechaNacimiento"
                onChange={handleChangeValueForm}
              />
            </div>
          </div>

          <div className="flex flex-wrap gap-5">
            <div className="flex flex-wrap">
              <Typography variant="h6" color="blue-gray">
                Correo electrónico:
              </Typography>
              <Input
                size="md"
                disabled={noEditar}
                name="correoElectronico"
                value={informacionUsuario.correoElectronico}
                onChange={handleChangeValueForm}
              />
            </div>
          </div>
          {noEditar ? (
            <div className="flex justify-between flex-wrap gap-2 mt-5">
              <Button onClick={habilitarComponentes}>Editar Perfil</Button>
              <Button onClick={handleOpenChangePasssword} className="bg-[#0000de] ">Cambiar Contraseña</Button>
            </div>
          ) : (
            <div className="flex justify-between flex-wrap gap-2 mt-5">
              <Button className="bg-[#0000de]" onClick={actualizarUsuario}>
                Guardar
              </Button>
              <Button color="red" onClick={() => setNoEditar(true)} >Cancelar</Button>
            </div>
          )}
        </CardBody>
      </Card>
      <ChangePassword show={modalForm.toggle} handleOpen={handleOpenChangePasssword} idUsuario={informacionUsuario.id}/>
    </div>
  );
}

export default ProfilePage;
