"use client";
import Register from "@/components/account/Register";

function Signup() {
  
  return (
    <div className="flex w-full ">
      <div className="w-full flex  justify-center">
        <div className="bg-white px-5 py-20 rounded-3xl border-2 border-gray-100">
          <Register/>
        </div>
        <div className="flex justify-center items-center">
          <img src="./images/resetPassword.jpg" alt="logo" width={600} />
        </div>
      </div>
    </div>
  );
}

export default Signup;
