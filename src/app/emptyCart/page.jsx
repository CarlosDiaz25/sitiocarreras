"use client";

import { EyeSlashIcon } from "@heroicons/react/24/solid";
import { Typography } from "@material-tailwind/react";
import Link from "next/link";

function CartEmptyPage() {
  return (
    <div className="flex w-full h-screen justify-center items-center">
      <EyeSlashIcon className="h-24 w-24 text-blue-800" />
      <div className="flex flex-col items-center">
        <Typography variant="h4" color="blue-gray">
          Su carrito esta vacío
        </Typography>
        <Link href={"/"} passHref>
          <Typography variant="h4" color="light-blue">
            Regresar
          </Typography>
        </Link>
      </div>
    </div>
  );
}
export default CartEmptyPage;
