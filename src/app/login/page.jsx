"use client";

import Login from "@/components/account/Login";
import RecoverAccount from "@/components/account/RecoverAccount";
import {
  ArrowPathIcon,
  UserCircleIcon,
  UserPlusIcon,
} from "@heroicons/react/24/solid";
import {
  Card,
  Tabs,
  TabsHeader,
  TabsBody,
  Tab,
  TabPanel,
} from "@material-tailwind/react";

const Signin = () => {
  return (
    <div className="flex w-full ">
      <div className="w-full flex  justify-center">
        <div className="bg-white px-5 py-20 rounded-3xl border-2 border-gray-100">
          <Card color="transparent" shadow={false}>
            <Tabs value="login">
              <TabsHeader>
                <Tab key="login" value="login">
                  <div className="flex items-center gap-2">
                    <UserCircleIcon className="w-5 h-5" />
                    <label>Iniciar sesión</label>
                  </div>
                </Tab>
                <Tab key="recover" value="recover">
                  <div className="flex items-center gap-2">
                    <ArrowPathIcon className="w-5 h-5" />
                    <label>Recuperar contraseña</label>
                  </div>
                </Tab>
              </TabsHeader>
              <TabsBody>
                <TabPanel key="login" value="login">
                  <Login />
                </TabPanel>
                <TabPanel key="recover" value="recover">
                  <RecoverAccount />
                </TabPanel>
              </TabsBody>
            </Tabs>
          </Card>
        </div>
        <div>
          <img src="./images/login.jpg" alt="logo" width={450} />
        </div>
      </div>
    </div>
  );
};

export default Signin;
