"use client";

import CarroselSection from '@/components/Carrosel';
import { Categorias } from '@/components/Categorias';
import InfoGeneral from '@/components/InfoGeneral';
import InformacionPaquete from '@/components/InformacionPaquete';
import Patrocinadores from '@/components/Patrocinadores';
import { NotificationCustom } from '@/components/ui/NotificationCustom';
import { getCarrouselForCareer } from '@/services/carreras';
import React, { useEffect, useState } from 'react'

export default function Page({ params }) {

  const { idcarrera } = params;
  const [carrousel, setCarrousel] = useState([]);


  useEffect(() => {
    obtenerCarrousel();
  }, []);

  const obtenerCarrousel = async () => {
    try {
      const response = await getCarrouselForCareer(idcarrera);
      if (response.resultado) 
        setCarrousel(response.data);

    } catch (error) {
      console.log(error);
      NotificationCustom({
        type: "error",
        message: "Ocurrió un error al obtener las carreras",
      });
    }
  };

  return (
    <main>
     <CarroselSection carreras={carrousel} allCarer="false"/>
     <Categorias idcarrera={idcarrera} />
     <InformacionPaquete idcarrera={idcarrera} />
     <InfoGeneral idcarrera={idcarrera} />
     <Patrocinadores idcarrera={idcarrera} />
     </main>
  )
}
