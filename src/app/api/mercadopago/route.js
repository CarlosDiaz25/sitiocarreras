import { NextResponse } from "next/server";

const mercadopago = require("mercadopago");

export  async function POST( request ){
	mercadopago.configure({
		access_token: `${process.env.NEXT_PUBLIC_MERCADO_PAGO_CLIENT_ID}`,
	});

	let body = await request.json();

	let preference = {
		items: body.items,
		back_urls: {
			"success": "http://localhost:3000/cart",
			"failure": "http://localhost:3000/cart",
			"pending": "http://localhost:3000/cart",
		},
		auto_return:"approved",
		notification_url: "https://fb1c-187-190-214-154.ngrok-free.app/api/webhook",
		metadata: {
			detalles:body.detalles,
			usuarioId:body.usuarioId
		}
	};

	const result = await mercadopago.preferences.create(preference);

	return NextResponse.json(result);
}

