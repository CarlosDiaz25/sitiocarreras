import { NextResponse } from "next/server";
import { existeCompra, saveCompra, verficarEstatus } from "@/services/compras";
import { saveErrorMercadoPago } from "@/services/error";

const mercadopago = require("mercadopago");
const url = require("url");

export async function POST(request) {
  mercadopago.configure({
    access_token: `${process.env.NEXT_PUBLIC_MERCADO_PAGO_CLIENT_ID}`,
  });

  const parsedUrl = url.parse(request.url, true);
  const payment = parsedUrl.query;

  if (payment.type === "payment") {
    const data = await mercadopago.payment.findById(payment["data.id"]);

    if (data.body.status === "refunded" || data.body.status === "cancelled") {
      const response = await verficarEstatus({ order: payment["data.id"] });
      if (!response.resultado) {
        console.log("Ocurrio un error al validar el status" + response.data);
      }
    }

    if (data.body.status === "approved") {
      const response = await existeCompra(payment["data.id"]);
      if (response.resultado) {
        const responseVeri = await verficarEstatus({
          order: payment["data.id"],
        });
        if (responseVeri.resultado) {
          return NextResponse.json({ status: 204 });
        }
        console.log("Ocurrio un error al validar el status " + responseVeri.data);
        return NextResponse.json({ status: 204 });
      }
      console.log("Ocurrio un error al validar si existe la compra" + response.data);
    }

    if (
      data.body.status === "approved" ||
      data.body.status === "pending" ||
      data.body.status === "in_process"
    ) {
      const item = [];
      data.body.metadata.detalles.forEach((element) => {
        item.push({
          participanteId: element.participante_id,
          tallaId: element.talla_id,
          categoriaTipoCarreraId: element.categoria_tipo_carrera_id,
          capacidadDiferente: element.capacidad_diferente,
        });
      });

      const body = {
        usuarioId: data.body.metadata.usuario_id,
        formaPago: 1,
        orderId: payment["data.id"],
        detalles: item,
      };

      const response = await saveCompra(body);

      if (!response.resultado) {
        const errorstatus = await saveErrorMercadoPago({
          usuarioId: data.body.metadata.usuario_id,
          mensaje: response.data,
          body: JSON.stringify(body),
          pagoId: payment["data.id"],
        });

        console.log(
          "Ocurrio un error y se registro: " +
            errorstatus.resultado +
            " " +
            errorstatus.data ?? errorstatus.data
        );
      }
    }

    return NextResponse.json({ status: 204 });
  }
  return NextResponse.json({ status: 204 });
}
