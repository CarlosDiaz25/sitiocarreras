import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import { login } from "@/services/usuario";

const handler = NextAuth({
  providers: [
    CredentialsProvider({
      name: "Credentials",
      id: "credentials",
      credentials: {
        correo: { name: "correo", label: "Correo", type: "text" },
        contrasenia: {
          name: "contrasenia",
          label: "Contraseña",
          type: "password",
        },
      },
      async authorize(credentials) {
        const usuario = await login(credentials);

        if (!usuario.resultado)
          throw new Error("Usuario o contraseña invalidos");

        return usuario.data;
      },
    }),
  ],
  pages: {
    signIn: "/login",
  },
  session: {
    strategy: "jwt",
    maxAge: 2592000,
    updateAge: 86400,
  },
  callbacks: {
    async jwt({ token, user, trigger, session }) {
      if (trigger === "update") {
        
        if (session.user) token.user = session.user;
        return token;
      }

      if (user) token.user = user;
      return token;
    },
    async session({ session, token }) {
      session.user = token.user;
      return session;
    },
  },
});

export { handler as GET, handler as POST };
