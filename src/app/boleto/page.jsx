"use client";

import {
  Checkbox,
  Button,
  Typography,
  Tabs,
  TabsHeader,
  TabsBody,
  Tab,
  TabPanel,
  Chip,
} from "@material-tailwind/react";
import { useState, useEffect, useContext } from "react";
import {
  getParticipante,
  getSubCategorias,
} from "@/services/participante";
import { useSession } from "next-auth/react";

import { CurrencyDollarIcon, UserPlusIcon } from "@heroicons/react/24/solid";
import FormParticipante from "@/components/FormParticipante";
import { getTallas } from "@/services/talla";
import { CartContext } from "@/context/cart";
import Link from "next/link";
import { validarCompra } from "@/services/compras";
import { useRouter } from "next/navigation";
import { NotificationCustom } from "@/components/ui/NotificationCustom";
import { getCarreras } from "@/services/carreras";
import { getTypeCareer } from "@/services/tipoCarreras";

function BoletoPage() {
  const { data: session, status } = useSession();
  const { addFichastoCart, descuento } = useContext(CartContext);
  const [dataParticipantes, setDataParticipantes] = useState([]);
  const [dataTallas, setDataTallas] = useState([
    { id: 0, nombre: "Selecciona una talla" },
  ]);
  const [dataCarreras, setDataCarreras] = useState([
    { id: 0, nombre: "Selecciona una carrera" },
  ]);
  const [dataCategorias, setDataCategorias] = useState([
    { id: 0, nombre: "Selecciona una categoria" },
  ]);
  const [dataSubCategorias, setDataSubCategorias] = useState([
    { id: 0, nombre: "Selecciona una subcategoria" },
  ]);
  const [actualState, changeCheckState] = useState(false);
  const [actualCheckCpDiferentes, setActualCheckCpDiferentes] = useState(false);
  const [mostrarBtnCarrito, setMostrarBtnCarrito] = useState(false);
  const [mostrarChip, setMostrarChip] = useState(false);
  const router = useRouter();

  const [data, setData] = useState({
    carrera: {},
    participante: {},
    talla: {},
    categoria: {},
    subcategoria: {},
    usuarioId: "",
    capacidadDiferente: "",
  });

  useEffect(() => {
    if (status === "authenticated") {
      obtenerCarreras();
      obtenerParticipantes(session.user);
    } else if (status === "unauthenticated") {
      router.replace("/login");
    }
  }, [status]);

  useEffect(() => {
    if (data.carrera.id > 0) {
      obtenerTallas(data.carrera.id);

      setData({ ...data, participante: {} });
      let combo = document.getElementById("select-participantes");
      combo.selectedIndex = 0;

      let comboTalla = document.getElementById("select-talla");
      comboTalla.selectedIndex = 0;

      let comboSubCategoria = document.getElementById("select-subcategoria");
      comboSubCategoria.selectedIndex = 0;

      setMostrarChip(false);
    }
  }, [data.carrera]);

  useEffect(() => {
    if (data.carrera.id > 0) obtenerCategorias(data.carrera.id);
  }, [data.participante]);

  useEffect(() => {
    if (data.categoria.id > 0)
      obtenerSubCategortias(data.participante.id, data.categoria.id);
  }, [data.categoria]);

  const obtenerCarreras = async () => {
    try {
      const opciones = [];
      opciones.push({ id: 0, nombre: "Selecciona una carrera" });

      const listCarreras = await getCarreras();

      if (listCarreras.resultado) {
        listCarreras.data.forEach((element) => {
          opciones.push(element);
        });

        setDataCarreras(opciones);
      }
    } catch (error) {
      NotificationCustom({
        type: "error",
        message: "Ocurrio un error al obtener las carreras",
      });
    }
  };

  const obtenerParticipantes = async (usuario) => {
    try {
      const opciones = [];
      opciones.push({ id: 0, nombre: "Selecciona un participante" });
      const listParticipantes = await getParticipante(usuario.id);
      if (listParticipantes.resultado) {
        listParticipantes.data.forEach((element) => {
          opciones.push(element);
        });
        setDataParticipantes(opciones);
      }
    } catch (error) {
      NotificationCustom({
        type: "error",
        message: "Ocurrio un error al obtener los participantes",
      });
    }
  };

  const obtenerTallas = async (idCarrera) => {
    try {
      const opciones = [];
      opciones.push({ id: 0, nombre: "Selecciona una talla" });

      const listTallas = await getTallas(idCarrera);

      if (listTallas.resultado) {
        listTallas.data.forEach((element) => {
          opciones.push(element);
        });
        setDataTallas(opciones);
      }
    } catch (error) {
      NotificationCustom({
        type: "error",
        message: "Ocurrio un error al obtener las tallas",
      });
    }
  };

  const obtenerCategorias = async (idCarrera) => {
    try {
      const opciones = [];

      opciones.push({ id: 0, nombre: "Selecciona una categoria" });

      var comboSubCategoria = document.getElementById("select-subcategoria");
      comboSubCategoria.selectedIndex = 0;
      setData({ ...data, categoria: {} });
      changeCheckState(false);

      if (idCarrera > 0) {
        const listCategorias = await getTypeCareer(idCarrera);

        listCategorias.data.forEach((element) => {
          opciones.push(element);
        });

        if (listCategorias.resultado) {
          setDataCategorias(opciones);
        }
      } else {
        setDataCategorias(opciones);
      }
    } catch (error) {
      console.log(error);
      NotificationCustom({
        type: "error",
        message: "Ocurrio un error al obtener las categorias",
      });
    }
  };

  const obtenerSubCategortias = async (participanteId, idTipoCarrera) => {
    try {
      const opciones = [];
      
      opciones.push({ id: 0, nombre: "Selecciona una subcategoria" });

      changeCheckState(false);

      var comboSubCategoria = document.getElementById("select-subcategoria");
      comboSubCategoria.selectedIndex = 0;
      setData({ ...data, subcategoria: {} });

      if (participanteId > 0 && idTipoCarrera > 0) {
        const listSubCategorias = await getSubCategorias(
          participanteId,
          idTipoCarrera
        );

        listSubCategorias.data.forEach((element) => {
          opciones.push(element);
        });

        if (listSubCategorias.resultado) {
          setDataSubCategorias(opciones);
        }

        if (opciones.length > 1) setMostrarChip(false);
        else setMostrarChip(true);
      } else {
        setDataSubCategorias(opciones);
      }
    } catch (error) {
      console.log(error);
      NotificationCustom({
        type: "error",
        message: "Ocurrio un error al obtener las subcategorias",
      });
    }
  };

  //Obtiene el participante registrado
  const handleOnRegister = (participante) => {
    setDataParticipantes([...dataParticipantes, participante]);
  };

  const handleChangeValueFormSelectParticipante = () => {
    var combo = document.getElementById("select-participantes");
    var selected = combo.options[combo.selectedIndex];

    setData({
      ...data,
      talla: {},
      subcategoria: {},
      participante: dataParticipantes.find(
        (participante) => participante.id.toString() === selected.value
      ),
    });

 
    let comboTalla = document.getElementById("select-talla");
    comboTalla.selectedIndex = 0;

    let comboCategoria = document.getElementById("select-categoria");
    comboCategoria.selectedIndex = 0;

    let comboSubCategoria = document.getElementById("select-subcategoria");
    comboSubCategoria.selectedIndex = 0;

  };

  const handleChangeValueFormSelectTalla = () => {
    var combo = document.getElementById("select-talla");
    var selected = combo.options[combo.selectedIndex];
    setData({
      ...data,
      talla: dataTallas.find((talla) => talla.id.toString() === selected.value),
    });
  };

  const handleChangeValueFormSelectCarrera = () => {
    var combo = document.getElementById("select-carrera");
    var selected = combo.options[combo.selectedIndex];

    const carreraSeleccionada = dataCarreras.find(
      (carrera) => carrera.id.toString() === selected.value
    );
    setData({
      ...data,
      carrera: {
        id: carreraSeleccionada.id,
        nombre: carreraSeleccionada.nombre,
        rutaImagen: carreraSeleccionada.rutaImagen,
        comision: carreraSeleccionada.comision,
        descuento: 0,
        descuentoAplicado: 0,
        capacidadDiferente: false,
      },
    });
  };

  const handleChangeValueFormSelectCategoria = () => {
    var combo = document.getElementById("select-categoria");
    var selected = combo.options[combo.selectedIndex];

    const categoriaSeleccionada = dataCategorias.find(
      (categoria) => categoria.id.toString() === selected.value
    );

    setData({
      ...data,
      categoria: {
        id: categoriaSeleccionada.id,
        nombre: categoriaSeleccionada.nombre,
        ruta: categoriaSeleccionada.ruta,
      },
    });
  };

  const handleChangeValueFormSelectSubCategoria = () => {
    var combo = document.getElementById("select-subcategoria");
    var selected = combo.options[combo.selectedIndex];

    const subCategoriaSeleccionada = dataSubCategorias.find(
      (subcategoria) => subcategoria.id.toString() === selected.value
    );

    setData({
      ...data,
      subcategoria: {
        id: subCategoriaSeleccionada.id,
        nombre: subCategoriaSeleccionada.nombre,
        costo: subCategoriaSeleccionada.costo,
      },
    });
  };

  const handleChexbox = (e) => {
    changeCheckState(e.target.checked);
  };

  const handleSubmit = async () => {
    try {
      if (!data.carrera.id) {
        NotificationCustom({
          type: "info",
          message: "Favor de seleccionar una carrera",
        });
        return;
      }

      if (!data.participante.id) {
        NotificationCustom({
          type: "info",
          message: "Favor de seleccionar un participante",
        });
        return;
      }

      if (!data.talla.id) {
        NotificationCustom({
          type: "info",
          message: "Favor de seleccionar una talla",
        });
        return;
      }

      if (!data.categoria.id) {
        NotificationCustom({
          type: "info",
          message: "Favor de seleccionar una categoria",
        });
        return;
      }

      if (!data.subcategoria.id) {
        NotificationCustom({
          type: "info",
          message: "Favor de seleccionar una subcategoria",
        });
        return;
      }
      
      if (!actualState) {
        NotificationCustom({
          type: "info",
          message: "Favor de acpetar los terminos",
        });
        return;
      }

      const validacion = {
        participanteId: data.participante.id,
        tallaId: data.talla.id,
        categoriaTipoCarreraId: data.subcategoria.id,
        usuarioId: session.user.id,
        idCarrera: data.carrera.id,
      };

      const validacionParticipante = await validarCompra(validacion);

      if (!validacionParticipante.resultado) {
        NotificationCustom({
          type: "info",
          message: validacionParticipante.data,
        });
        return;
      }

      const result = addFichastoCart({
        participante: data.participante,
        talla: data.talla,
        carrera: data.carrera,
        categoria: data.categoria,
        subcategoria: data.subcategoria,
        usuarioId: session.user.id,
        capacidadDiferente: data.capacidadDiferente,
      });

      if (result) {
        NotificationCustom({
          type: "info",
          message: result,
        });
        return;
      }

      NotificationCustom({
        type: "success",
        message: "El participante se agregó al carrito de compras",
      });

      onClear();

      setMostrarBtnCarrito(true);
    } catch (error) {
      NotificationCustom({
        type: "error",
        message: "Ocurrio un error al agregar al carrito",
      });
    }
  };

  const onClear = () => {
    try {
      setData({
        participante: {},
        talla: {},
        carrera: {},
        categoria: {},
        subcategoria: {},
        capacidadDiferente: "",
      });

      let combo = document.getElementById("select-participantes");
      combo.selectedIndex = 0;

      let comboTalla = document.getElementById("select-talla");
      comboTalla.selectedIndex = 0;

      let comboCarrera = document.getElementById("select-carrera");
      comboCarrera.selectedIndex = 0;

      let comboCategoria = document.getElementById("select-categoria");
      comboCategoria.selectedIndex = 0;

      let comboSubCategoria = document.getElementById("select-subcategoria");
      comboSubCategoria.selectedIndex = 0;

      setActualCheckCpDiferentes(false);
      changeCheckState(false);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="flex w-full ">
      <div className="w-full flex  justify-center">
        <div className="bg-white px-5 py-20 rounded-3xl border-2 border-gray-100">
          <Typography variant="h4" color="blue-gray">
            Detalles del participante
          </Typography>
          <Typography color="gray" className="mt-1 font-normal">
            Favor de ingresar los datos solicitados
          </Typography>
          <Tabs value="boleto">
            <TabsHeader>
              <Tab key={"boleto"} value={"boleto"}>
                <div className="flex items-center gap-2">
                  <CurrencyDollarIcon className="w-5 h-5" />
                  <label>Adquirir ficha</label>
                </div>
              </Tab>
              <Tab key={"onRegister"} value={"onRegister"}>
                <div className="flex items-center gap-2">
                  <UserPlusIcon className="w-5 h-5" />
                  <label>Participante</label>
                </div>
              </Tab>
            </TabsHeader>
            <TabsBody>
              <TabPanel key={"boleto"} value={"boleto"}>
                <form className="mt-8  w-80 max-w-screen-lg sm:w-96">
                  <div className="mb-4 flex flex-col gap-6">
                    <div className="relative h-10 ">
                      <select
                        id="select-carrera"
                        name="select-carrera"
                        onChange={handleChangeValueFormSelectCarrera}
                        className="peer h-full w-full rounded-[7px] border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-2.5 font-sans text-sm font-normal text-blue-gray-700 
                               outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200
                             focus:border-2 focus:border-black focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50"
                      >
                        {dataCarreras.map((option) => (
                          <option key={option.id} value={option.id}>
                            {option.nombre}
                          </option>
                        ))}
                      </select>
                      <label
                        className="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight
                             text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 
                              before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] 
                              after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r 
                              after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[3.75] 
                              peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent 
                              peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-black peer-focus:before:border-t-2 peer-focus:before:border-l-2 
                              peer-focus:before:border-black peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:border-black 
                              peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500"
                      >
                        Selecciona una carrera
                      </label>
                    </div>

                    <div className="relative h-10 ">
                      <select
                        id="select-participantes"
                        name="select-participantes"
                        onChange={handleChangeValueFormSelectParticipante}
                        value={data.participante.id}
                        className="peer h-full w-full rounded-[7px] border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-2.5 font-sans text-sm font-normal text-blue-gray-700 
                               outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200
                              focus:border-2 focus:border-black focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50"
                      >
                        {dataParticipantes.map((option) => (
                          <option key={option.id} value={option.id}>
                            {option.nombre} {option.apellidoPaterno}{" "}
                            {option.apellidoMaterno}
                          </option>
                        ))}
                      </select>
                      <label
                        className="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight
                             text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 
                              before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] 
                              after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r 
                              after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[3.75] 
                              peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent 
                              peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-black peer-focus:before:border-t-2 peer-focus:before:border-l-2 
                              peer-focus:before:border-black peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:border-black 
                              peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500"
                      >
                        Selecciona un participante
                      </label>
                    </div>

                    <div className="relative h-10 ">
                      <select
                        id="select-talla"
                        name="select-talla"
                        onChange={handleChangeValueFormSelectTalla}
                        className="peer h-full w-full rounded-[7px] border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-2.5 font-sans text-sm font-normal text-blue-gray-700 
                               outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200
                              focus:border-2 focus:border-black focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50"
                      >
                        {dataTallas.map((option) => (
                          <option key={option.id} value={option.id}>
                            {option.nombre}
                          </option>
                        ))}
                      </select>
                      <label
                        className="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight
                             text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 
                              before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] 
                              after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r 
                              after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[3.75] 
                              peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent 
                              peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-black peer-focus:before:border-t-2 peer-focus:before:border-l-2 
                              peer-focus:before:border-black peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:border-black 
                              peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500"
                      >
                        Selecciona una talla
                      </label>
                    </div>

                    <div className="relative h-10 ">
                      <select
                        id="select-categoria"
                        name="select-categoria"
                        onChange={handleChangeValueFormSelectCategoria}
                        className="peer h-full w-full rounded-[7px] border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-2.5 font-sans text-sm font-normal text-blue-gray-700 
                               outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200
                             focus:border-2 focus:border-black focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50"
                      >
                        {dataCategorias.map((option) => (
                          <option key={option.id} value={option.id}>
                            {option.nombre}
                          </option>
                        ))}
                      </select>
                      <label
                        className="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight
                             text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 
                              before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] 
                              after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r 
                              after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[3.75] 
                              peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent 
                              peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-black peer-focus:before:border-t-2 peer-focus:before:border-l-2 
                              peer-focus:before:border-black peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:border-black 
                              peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500"
                      >
                        Selecciona una categoria
                      </label>
                    </div>

                    <div className="relative h-10 ">
                      <select
                        id="select-subcategoria"
                        name="select-subcategoria"
                        onChange={handleChangeValueFormSelectSubCategoria}
                        className="peer h-full w-full rounded-[7px] border border-blue-gray-200 border-t-transparent bg-transparent px-3 py-2.5 font-sans text-sm font-normal text-blue-gray-700 
                               outline outline-0 transition-all placeholder-shown:border placeholder-shown:border-blue-gray-200 placeholder-shown:border-t-blue-gray-200
                             focus:border-2 focus:border-black focus:border-t-transparent focus:outline-0 disabled:border-0 disabled:bg-blue-gray-50"
                      >
                        {dataSubCategorias.map((option) => (
                          <option key={option.id} value={option.id}>
                            {option.nombre}
                          </option>
                        ))}
                      </select>
                      <label
                        className="before:content[' '] after:content[' '] pointer-events-none absolute left-0 -top-1.5 flex h-full w-full select-none text-[11px] font-normal leading-tight
                             text-blue-gray-400 transition-all before:pointer-events-none before:mt-[6.5px] before:mr-1 before:box-border before:block before:h-1.5 before:w-2.5 
                              before:rounded-tl-md before:border-t before:border-l before:border-blue-gray-200 before:transition-all after:pointer-events-none after:mt-[6.5px] 
                              after:ml-1 after:box-border after:block after:h-1.5 after:w-2.5 after:flex-grow after:rounded-tr-md after:border-t after:border-r 
                              after:border-blue-gray-200 after:transition-all peer-placeholder-shown:text-sm peer-placeholder-shown:leading-[3.75] 
                              peer-placeholder-shown:text-blue-gray-500 peer-placeholder-shown:before:border-transparent peer-placeholder-shown:after:border-transparent 
                              peer-focus:text-[11px] peer-focus:leading-tight peer-focus:text-black peer-focus:before:border-t-2 peer-focus:before:border-l-2 
                              peer-focus:before:border-black peer-focus:after:border-t-2 peer-focus:after:border-r-2 peer-focus:after:border-black 
                              peer-disabled:text-transparent peer-disabled:before:border-transparent peer-disabled:after:border-transparent peer-disabled:peer-placeholder-shown:text-blue-gray-500"
                      >
                        Selecciona una subcategoria
                      </label>
                    </div>
                  </div>

                  {mostrarChip ? (
                    <>
                      <Chip
                        variant="ghost"
                        color="red"
                        size="sm"
                        value="No se encontraron carreras disponibles "
                        className="mb-2 flex-wrap"
                        icon={
                          <span className="mx-auto mt-1 block h-2 w-2 rounded-full bg-red-900 content-['']" />
                        }
                      />
                      <Chip
                        variant="ghost"
                        color="red"
                        size="sm"
                        value="para este participante en la categoria seleccionada"
                        className="mb-2 flex-wrap"
                      />
                    </>
                  ) : (
                    <></>
                  )}

                  {data.subcategoria.id ? (
                    <>
                      <div className="flex flex-col mb-5">
                        <div className="flex justify-between">
                          <Typography variant="h4" color="blue-gray">
                            Costo de la ficha:
                          </Typography>
                          <Typography variant="h4" color="blue">
                            {`$${
                              data.subcategoria.costo -
                              (data.carrera.descuentoAplicado
                                ? data.carrera.descuentoAplicado
                                : 0)
                            }`}
                          </Typography>
                        </div>
                      </div>
                    </>
                  ) : (
                    <></>
                  )}

                  <div className="flex">
                    <Checkbox
                      width={10}
                      checked={actualState}
                      onChange={handleChexbox}
                      containerProps={{ className: "-ml-2.5" }}
                      color="blue"
                    />
                    <Typography
                      variant="small"
                      color="gray"
                      className="flex items-center font-normal"
                    >
                      En uso de mis facultades certifico que mi participación en
                      esta carrera es bajo mi completa y total responsabilidad,
                      por lo que el comité organizador no podrá ser sujeto a
                      reclamación legal alguna a mi favor
                    </Typography>
                  </div>

                  <Button
                    className="mt-6 bg-[#0000de]"
                    onClick={handleSubmit}
                    fullWidth
                  >
                    Agregar al carrito
                  </Button>
                  {mostrarBtnCarrito ? (
                    <Link href="/cart" passHref>
                      <Button className="mt-6" color="red" fullWidth>
                        Ir al carrito
                      </Button>
                    </Link>
                  ) : (
                    <></>
                  )}
                </form>
              </TabPanel>
              <TabPanel key={"onRegister"} value={"onRegister"}>
                <FormParticipante onRegister={handleOnRegister} />
              </TabPanel>
            </TabsBody>
          </Tabs>
        </div>
        {data.carrera.id > 0 ? (
          <div className="hidden lg:flex md:flex flex-col justify-center items-center">
            <Typography variant="h6">{data.carrera.nombre}</Typography>
            <img
              src={data.carrera.rutaImagen}
              alt="logo"
              width={500}
              className="m-5 rounded-md shadow-2xl"
            />
          </div>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}

export default BoletoPage;
