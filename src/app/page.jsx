"use client";

import { CardSliderCarreras } from "@/components/CardSliderCarreras";
import CarroselSection from "@/components/Carrosel";
import { FormContactanos } from "@/components/FormContactanos";
import { ReviewsCard } from "@/components/ReviewsCard";
import { NotificationCustom } from "@/components/ui/NotificationCustom";
import { getCarrousel } from "@/services/carreras";
import { useEffect, useState } from "react";

export default function Home() {
  const [carrousel, setCarrousel] = useState([]);

  useEffect(() => {
    obtenerCarrousel();
  }, []);

  const obtenerCarrousel = async () => {
    try {
      const response = await getCarrousel();

      if (response.resultado) setCarrousel(response.data);
    } catch (error) {
      console.log(error);
      NotificationCustom({
        type: "error",
        message: "Ocurrió un error al obtener las carreras",
      });
    }
  };

  return (
    <main>
      <CarroselSection carreras={carrousel} />
      <CardSliderCarreras carreras={carrousel} />
      <ReviewsCard />
      <FormContactanos />
    </main>
  );
}
