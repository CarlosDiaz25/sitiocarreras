export { default } from "next-auth/middleware";

export const config = {
    matcher : ['/cart','/about','/orders','/perfil','/emptyCart']
}