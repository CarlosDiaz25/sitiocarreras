
export const format = ( value ) => {
    const formatter = Intl.NumberFormat('es-MX', {
        style: 'currency',
        currency:"MXN",
        minimumFractionDigits:2,
        maximumFractionDigits:2
    })

    return formatter.format( value );
}